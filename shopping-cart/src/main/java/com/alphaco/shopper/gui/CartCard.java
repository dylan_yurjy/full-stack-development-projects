package com.alphaco.shopper.gui;

import com.alphaco.shopper.entitys.Product;

import javax.swing.*;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static com.alphaco.shopper.gui.Swing.*;
import static com.alphaco.shopper.gui.Utils.*;

public class CartCard extends JPanel {

    protected static int numCartCards;
    private final CartPanel cartPanel;
    private final ConfigPanel configPanel;
    private final Product product;
    private final int index;
    private JLabel stockLabel;
    private int qty;

    public CartCard(CartPanel cartPanel, Product product, int cartScrollPanelWidth, int scrollBarWidth, int qty) {
        this.configPanel = cartPanel.getConfigPanel();
        this.cartPanel = cartPanel;
        this.product = product;
        this.index = numCartCards;
        this.qty = qty;
        numCartCards++;
        initialize(cartScrollPanelWidth, scrollBarWidth);
    }

    private void initialize(int cartScrollPanelWidth, int scrollBarWidth) {
        int cardHeight = 80;
        this.setSize(cartScrollPanelWidth - 60, cardHeight);
        int locationX = (cartScrollPanelWidth - this.getWidth() - scrollBarWidth) / 2;
        int locationY = (cardHeight * index) + (index * 20) + 40;
        this.setLayout(null);
        this.setLocation(locationX, locationY);

        ImageIcon imageIcon = getCartIcon(product.getThumbnail_url());
        JLabel itemLabel = new JLabel(product.getTitle());
        JLabel picture = new JLabel(imageIcon);
        JLabel priceLabel = new JLabel(String.format("%s",product.getPrice()));
        BasicArrowButton upButton = new BasicArrowButton(BasicArrowButton.NORTH);
        BasicArrowButton downButton = new BasicArrowButton(BasicArrowButton.SOUTH);
        stockLabel = new JLabel(String.valueOf(qty));


        itemLabel.setFont(brandFont);
        itemLabel.setSize(itemLabel.getPreferredSize());
        itemLabel.setLocation(((this.getWidth()) - itemLabel.getWidth()) / 2, 1);

        picture.setSize(50, 50);
        picture.setLocation(10, (this.getHeight() - picture.getHeight()) / 2 + 7);

        priceLabel.setFont(descFont);
        priceLabel.setSize(priceLabel.getPreferredSize());
        priceLabel.setLocation(
                (this.getWidth() - priceLabel.getWidth()) / 2 + (int) (this.getWidth() * .25) + 2,
                (this.getHeight() - priceLabel.getHeight()) / 2 + 2);

        upButton.setSize(15, 15);
        upButton.setLocation((this.getWidth() - upButton.getWidth()) / 2, 22);

        stockLabel.setFont(descFont);
        stockLabel.setSize(stockLabel.getPreferredSize());
        stockLabel.setLocation((this.getWidth() - stockLabel.getWidth()) / 2 - 1, 39);

        downButton.setSize(15, 15);
        downButton.setLocation((this.getWidth() - downButton.getWidth()) / 2, 62);

        upButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int id = product.getId();
                if (configPanel.getProductsPanel().checkStock(id)) {
                        boolean response = incrementCartItemQty(id);
                        if (response) {
                            float amount = currencyParser(product.getPrice());
                            cartPanel.addToTotal(amount);
                            configPanel.getProductsPanel().updateStock(-1, product);
                            qty += 1;
                            reconfigureStockLabel();
                        } else {
                            for (var item : Thread.currentThread().getStackTrace()) {
                                System.out.println(item);
                            }
                        }
                } else {
                    JOptionPane.showMessageDialog(
                            cartPanel.getFrame(),
                            "Sorry! You've reached the limit for that item!");
                }
            }
        });

        downButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (qty != 1) {
                    boolean response = decrementCartItemQty(product.getId());
                    if (response) {
                        float amount = currencyParser(product.getPrice());
                        cartPanel.subtractFromTotal(amount);
                        configPanel.getProductsPanel().updateStock(1, product);
                        qty -= 1;
                        reconfigureStockLabel();
                    } else {
                        for (var item : Thread.currentThread().getStackTrace()) {
                            System.out.println(item);
                        }
                    }
                } else {
                    int result = JOptionPane.showConfirmDialog(
                            cartPanel.getFrame(),
                            "You only have one more of this item. Would you like to remove it from you cart?");
                    if (result == 0) {
                        List<?> cartList = deleteItemFromCart(product.getId());
                        if (cartList != null) {
                            float amount = currencyParser(product.getPrice());
                            cartPanel.subtractFromTotal(amount);
                            configPanel.getProductsPanel().updateStock(1, product);
                            cartPanel.refreshCart(cartList);
                        } else {
                            for (var item : Thread.currentThread().getStackTrace()) {
                                System.out.println(item);
                            }
                        }
                    }
                }
            }
        });

        this.add(picture);
        this.add(upButton);
        this.add(stockLabel);
        this.add(downButton);
        this.add(priceLabel);
        this.add(itemLabel);
        this.setBorder(BorderFactory.createSoftBevelBorder(SoftBevelBorder.RAISED));
    }

    private void reconfigureStockLabel() {
        stockLabel.setText(String.valueOf(qty));
        stockLabel.setSize(stockLabel.getPreferredSize());
        stockLabel.setLocation((stockLabel.getParent().getWidth() - stockLabel.getWidth()) / 2 - 1, 39);
    }

    public int getQty() {
        return qty;
    }

    public Product getProduct() {
        return product;
    }
}
