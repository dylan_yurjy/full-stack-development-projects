package com.alphaco.shopper.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

public class Utils {

    protected static final NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.getDefault());

    protected static ImageIcon getCartIcon(String URL) {
        try {
            java.net.URL imageUrl = new URL(URL);
            InputStream in = imageUrl.openStream();
            BufferedImage image = ImageIO.read(in);
            return new ImageIcon(image.getScaledInstance(50, 50, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static float currencyParser(String amount) {
        try {
            return formatter.parse(amount).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    protected static int currencyParserInt(String amount) {
        try {
            return formatter.parse(amount).intValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    protected static List<?> getProductList() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest
                    .newBuilder(new URI("http://localhost:8080/products/getAll"))
                    .GET()
                    .build();
            HttpResponse<byte[]> res = client.send(
                    httpRequest,
                    responseInfo -> HttpResponse.BodySubscribers.ofByteArray());
            byte[] bytes = res.body();
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
            objectInputStream.close();
            return (List<?>) objectInputStream.readObject();
        } catch (URISyntaxException | IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static List<?> getCartList() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest.newBuilder(
                            new URI("http://localhost:8080/getCart/Dylan"))
                    .GET()
                    .build();
            HttpResponse<byte[]> res = client.send(
                    httpRequest,
                    responseInfo -> HttpResponse.BodySubscribers.ofByteArray());
            byte[] bytes = res.body();
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
            objectInputStream.close();
            return (List<?>) objectInputStream.readObject();
        } catch (URISyntaxException | InterruptedException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static boolean clearCartRequest() {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest httpRequest = HttpRequest
                    .newBuilder(new URI("http://localhost:8080/getCart/Dylan/removeContents"))
                    .DELETE()
                    .build();
            HttpResponse<String> res = client.send(
                    httpRequest,
                    HttpResponse.BodyHandlers.ofString());
            return Boolean.parseBoolean(res.body());
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected static List<?> addToCart(int id) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest.BodyPublisher publisher = HttpRequest.BodyPublishers.noBody();
            HttpRequest httpRequest = HttpRequest.newBuilder(
                            new URI(
                                    String.format("http://localhost:8080/getCart/addProduct/Dylan/%s",
                                            id)))
                    .POST(publisher)
                    .build();
            HttpResponse<byte[]> res = client.send(
                    httpRequest,
                    responseInfo -> HttpResponse.BodySubscribers.ofByteArray());
            byte[] bytes = res.body();
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
            List<?> cartSet = (List<?>) objectInputStream.readObject();
            objectInputStream.close();
            return cartSet;
        } catch (URISyntaxException | IOException | ClassNotFoundException | InterruptedException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    protected static boolean incrementCartItemQty(int id) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest req = HttpRequest
                    .newBuilder(
                            new URI(
                                    String.format("http://localhost:8080/getCart/incrementProduct/Dylan/%s",
                                            id)))
                    .PUT(HttpRequest.BodyPublishers.noBody())
                    .build();
            HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
            return Boolean.parseBoolean(res.body());
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected static boolean decrementCartItemQty(int id) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest req = HttpRequest
                    .newBuilder(
                            new URI(
                                    String.format("http://localhost:8080/getCart/decrementProduct/Dylan/%s",
                                            id)))
                    .PUT(HttpRequest.BodyPublishers.noBody())
                    .build();
            HttpResponse<String> res = client.send(req, HttpResponse.BodyHandlers.ofString());
            return Boolean.parseBoolean(res.body());
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected static List<?> deleteItemFromCart(int id) {
        try {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest req = HttpRequest.newBuilder(
                            new URI(String.format("http://localhost:8080/getCart/removeProduct/Dylan/%s",
                                    id)))
                    .DELETE()
                    .build();
            HttpResponse<byte[]> res = client.send(req, responseInfo -> HttpResponse.BodySubscribers.ofByteArray());
            byte[] bytes = res.body();
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
            objectInputStream.close();
            return (List<?>) objectInputStream.readObject();
        } catch (URISyntaxException | InterruptedException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static ImageIcon getProductIcon(String URL) {
        try {
            java.net.URL imageUrl = new URL(URL);
            InputStream in = imageUrl.openStream();
            BufferedImage image = ImageIO.read(in);
            return new ImageIcon(image.getScaledInstance(150, 150, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}