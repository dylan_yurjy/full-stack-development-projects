package game.utils;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class ImageLoader {
    private final Image humanIcon;
    private final Image goblinIcon;
    private final Image dropIcon;
    private final Image treasureIcon;
    private final Image coinsIcon;
    private final Image skullsIcon;
    private final Image bronzeSwordIcon;
    private final Image bronzeSwordBigIcon;
    private final Image silverSwordIcon;
    private final Image silverSwordBigIcon;
    private final Image goldSwordIcon;
    private final Image goldSwordBigIcon;
    private final Image smallHealthPotionIcon;
    private final Image mediumHealthPotionIcon;
    private final Image largeHealthPotionIcon;


    public ImageLoader(Display display) {
        this.humanIcon = new Image(display, this.getClass().getResourceAsStream("/human.png"));
        this.goblinIcon = new Image(display, this.getClass().getResourceAsStream("/goblin.png"));
        this.dropIcon = new Image(display, this.getClass().getResourceAsStream("/drop.png"));
        this.treasureIcon = new Image(display, this.getClass().getResourceAsStream("/treasure.png"));
        this.coinsIcon = new Image(display, this.getClass().getResourceAsStream("/coins.png"));
        this.skullsIcon = new Image(display, this.getClass().getResourceAsStream("/goblinSkull.png"));
        this.bronzeSwordIcon = new Image(display, this.getClass().getResourceAsStream("/bronzeSword.png"));
        this.bronzeSwordBigIcon = new Image(display, this.getClass().getResourceAsStream("/bronzeSwordBig.png"));
        this.silverSwordIcon = new Image(display, this.getClass().getResourceAsStream("/silverSword.png"));
        this.silverSwordBigIcon = new Image(display, this.getClass().getResourceAsStream("/silverSwordBig.png"));
        this.goldSwordIcon = new Image(display, this.getClass().getResourceAsStream("/goldSword.png"));
        this.goldSwordBigIcon = new Image(display, this.getClass().getResourceAsStream("/goldSwordBig.png"));
        this.smallHealthPotionIcon = new Image(display, this.getClass().getResourceAsStream("/smallPotion.png"));
        this.mediumHealthPotionIcon = new Image(display, this.getClass().getResourceAsStream("/mediumPotion.png"));
        this.largeHealthPotionIcon = new Image(display, this.getClass().getResourceAsStream("/largePotion.png"));
    }

    public Image getHumanIcon() {
        return humanIcon;
    }

    public Image getGoblinIcon() {
        return goblinIcon;
    }

    public Image getDropIcon() {
        return dropIcon;
    }

    public Image getTreasureIcon() {
        return treasureIcon;
    }

    public Image getCoinsIcon() {
        return coinsIcon;
    }

    public Image getSkullsIcon() {
        return skullsIcon;
    }

    public Image getBronzeSwordIcon() {
        return bronzeSwordIcon;
    }

    public Image getBronzeSwordBigIcon() {
        return bronzeSwordBigIcon;
    }

    public Image getSilverSwordBigIcon() {
        return silverSwordBigIcon;
    }

    public Image getGoldSwordBigIcon() {
        return goldSwordBigIcon;
    }

    public Image getSilverSwordIcon() {
        return silverSwordIcon;
    }

    public Image getGoldSwordIcon() {
        return goldSwordIcon;
    }

    public Image getSmallHealthPotionIcon() {
        return smallHealthPotionIcon;
    }

    public Image getMediumHealthPotionIcon() {
        return mediumHealthPotionIcon;
    }

    public Image getLargeHealthPotionIcon() {
        return largeHealthPotionIcon;
    }
}
