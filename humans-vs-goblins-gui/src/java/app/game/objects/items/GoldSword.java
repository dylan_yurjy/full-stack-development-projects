package game.objects.items;

import game.Environment;

public class GoldSword extends AbstractWeapon {

    public GoldSword(Environment env) {
        super(50, "Gold Sword", env.getImageLoader().getGoldSwordIcon(), env.getImageLoader().getGoldSwordBigIcon());
    }
}
