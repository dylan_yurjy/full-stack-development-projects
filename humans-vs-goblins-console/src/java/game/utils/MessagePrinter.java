package game.utils;

import game.objects.Human;
import game.objects.Item;
//import game.objects.Land;

import java.util.ArrayList;
import java.util.HashMap;

import static game.utils.ConsoleColors.*;

public class MessagePrinter {

    private final HashMap<Integer, String> hudData = new HashMap<>();
    private final HashMap<Integer, Boolean> hudBoolean = new HashMap<>();

    {
        for (int i = 0; i < 15; i++) {
            hudData.put(i, "");
            hudBoolean.put(i, false);
        }
        setHudData(7, "Current Weapon: ", true);
        setHudData(14, "Enter a direction: w/d/s/a || Cycle Inventory: c || Use Item: r", true);
        setHudData(9, "Gold: 0", true);
    }

    public void printRowMessage(int index) {
        if (hudBoolean.get(index)) {
            System.out.print(hudData.get(index));
        }
    }

    public void setHudData(int index, String message, boolean active) {
        if (index == 13) {
            message = ANSI_RED + " " + message + ANSI_RESET;
        } else if (index == 3) {
            message = ANSI_YELLOW + " - " + message + ANSI_RESET;
        } else if (index == 0) {
            message = ANSI_GREEN + " - " + message + ANSI_RESET;
        } else {
            message = " - " + message;
        }
        hudData.replace(index, message);
        hudBoolean.replace(index, active);
    }

    public void generateInventoryList(Human human) {
        StringBuilder inventoryString = new StringBuilder("Inventory: ");
        ArrayList<Item> inv = human.getInventory();
        if (human.getSelected() == null && human.getInventory().size() > 0) {
            human.setSelected(human.getInventory().remove(0));
        }
        Item selected = human.getSelected();
        try {
            setHudData(11, String.format("Selected Item: %s",selected.getName()), true);
            inv.remove(selected);
        } catch (NullPointerException e) {
            setHudData(11, "Selected Item:", true);
        }
        for (int i = 0; i < human.getInventory().size(); i++) {
            if (i == inv.size() - 1) {
                inventoryString.append(inv.get(i).getName());
            } else {
                inventoryString.append(inv.get(i).getName());
                inventoryString.append(", ");
            }
        }
        setHudData(10, inventoryString.toString(), true);
    }

    public void updateGold(Human human) {
        setHudData(9, String.format("Current Gold: %s", human.getGold()), true);
    }

    public void generateWieldedList(Human human) {
        String message = String.format("Current Weapon: %s", human.getSetup().get(0).getName());
        setHudData(7, message, true);
    }

    public void setHudData(int index, boolean active) {
        hudBoolean.replace(index, active);
    }
}
