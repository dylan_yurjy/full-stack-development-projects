package hangman;

import static hangman.Functions.*;
import static hangman.Variables.*;

import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MainTest {

    @Test
    void updatePlayerWordTest() {
        computerWord = "hello";
        playerWord = "_____";
        guesses.get("correctGuesses").addAll(List.of(new String[]{"h", "e", "l"}));
        assertEquals(updatePlayerWord(), "HELL_");
    }

    @Test
    void testImportFunction() throws IOException {
        byte[] byteArray = this.getClass().getResourceAsStream("/art.txt").readAllBytes();
        String textFileAsString = new String(byteArray, StandardCharsets.UTF_8);
//        System.out.println(textFileAsString);
        String[] arrayOfGallows = textFileAsString.split("123456");
        int stage = 0;
        System.out.println(arrayOfGallows[stage]);

        stage++;

        System.out.println(arrayOfGallows[stage]);
    }

    @Test
    void resetGuesses() {
        System.out.println(guesses.toString());
        guesses.replaceAll((k, v) -> new ArrayList<>());
        System.out.println(guesses.toString());
    }
    
    @RepeatedTest(value = 50, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("wordGeneratorTest")
    void computerWordGeneratorTest() throws IOException {
        importData();
        assertNotNull(resources);
    }

    @Test
    void checkWinningFunctionality() {
        computerWord = "Hello";
        playerWord = "Hello";
        analyzeProgress();
        assertTrue(hasWon);
        playerWord = "Hell_";
        stage = 7;
        analyzeProgress();
        assertFalse(hasWon);
    }

    @RepeatedTest(value = 20)
    @DisplayName("Word Lengths Checker")
    void testGeneratedWords() throws IOException {
        importData();
        computerWord = chooseSecretWord(generateRandomIndex());
        playerWord = generatePlayerWord();
        assertEquals(playerWord.length(), computerWord.length());

    }

    @RepeatedTest(value = 100)
    @DisplayName("Random Index Generator")
    void indexGeneratorTest() {
        int x = generateRandomIndex();
        assertTrue(x >= 0 && x < 7);
    }

    @Test
    void fileReaderWriter() throws IOException {
        importData();
        System.out.println(scores);
    }

    @Test
    void checkScoresTest() throws IOException {
        importData();
        checkHighScore();
    }

    @Test
    void writeScoreToFileTest() throws IOException {
        importData();
        name = "Snuffleupagus";
        stage = 5;
        hasWon = true;
        writeScoreToFile();
    }
}