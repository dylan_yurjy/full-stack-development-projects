package game.objects.items;

import game.Environment;

public class LargeHealthPotion extends AbstractHealthPotion {
    private final String name = "Large Health Potion";

    public LargeHealthPotion(Environment env) {
        super(50);
        initiateCanvas(env.getImageLoader().getLargeHealthPotionIcon());
    }
}
