package game.objects.items;

import game.Environment;

public class SilverSword extends AbstractWeapon {

    public SilverSword(Environment env) {
        super(30, "Silver Sword", env.getImageLoader().getSilverSwordIcon(), env.getImageLoader().getSilverSwordBigIcon());
    }
}
