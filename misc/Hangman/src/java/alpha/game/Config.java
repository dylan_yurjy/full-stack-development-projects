package alpha.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Config {

    private static char[] word;
    public static char[] playerWord;
    private static int currentStage;
    private static boolean isPlaying;
    private static boolean isProgressing;
    private static boolean hasWon;
    public static HashMap<String, ArrayList<String>> guessMap = new HashMap<>();

    public static String getWord() {
        return String.join("", String.valueOf(word));
    }

    public static void setWords(char[] secret) {
        word = secret;
        playerWord = new char[word.length];
        Arrays.fill(playerWord, '_');
    }

    public static String getPlayerWord() {
        return String.join("", String.valueOf(playerWord));
    }

    public static void updatePlayerWord(int index, String letter) {
        playerWord[index] = letter.charAt(0);
    }

    public static void updatePlayerWord(char[] word) {
        playerWord = word;
    }

    public static HashMap<String, ArrayList<String>> getGuessMap() {
        return guessMap;
    }

    public static void updateGuessMap(String key, String letter) {
        switch (key) {
            case "incorrect" -> {
                guessMap.get("incorrectGuesses").add(letter);
                guessMap.get("totalGuesses").add(letter);
            }
            case "correct" -> {
                guessMap.get("correctGuesses").add(letter);
                guessMap.get("totalGuesses").add(letter);
            }
        }
    }

    public static void setGuessMap() {
        {
            guessMap.put("correctGuesses", new ArrayList<>());
            guessMap.put("incorrectGuesses", new ArrayList<>());
            guessMap.put("totalGuesses", new ArrayList<>());
        }
    }

    public static boolean isProgressing() {
        return isProgressing;
    }

    public static void setIsProgressing(boolean status) {
        isProgressing = status;
    }

    public static void setIsPlaying(boolean status) {
        isPlaying = status;
    }

    public static int getCurrentStage() {
        return currentStage;
    }

    public static void setCurrentStage(int currentStage) {
        Config.currentStage = currentStage;
    }

    public static boolean getHasWon() {
        return hasWon;
    }

    public static void setHasWon(boolean hasWon) {
        Config.hasWon = hasWon;
    }

    public static void initializeSettings() {
        setWords(WordLoader.secretWord().toCharArray());
        setCurrentStage(0);
        setIsPlaying(true);
        setIsProgressing(true);
        setHasWon(false);
        setGuessMap();
    }
}
