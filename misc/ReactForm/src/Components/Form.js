import React from 'react';
import '../css/form.css';

const Form = () => {
    return (
        <div className="container">
            <form action='#'>
                <label htmlFor="firstName">First Name</label>
                <input type="text" id="firstName" placeholder="required"></input>
                <label htmlFor="lastName">Last Name</label>
                <input type="text" id="lastName" placeholder="required"></input>
                <label htmlFor="email">Email</label>
                <input type="text" id="email" placeholder="required"></input>
                <label htmlFor="occupation">Occupation</label>
                <input type="text" id="occupation" placeholder="required"></input>
                <label htmlFor="country">Country</label>
                <input type="text" id="country" placeholder="required"></input>
                <div id="button-container">
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    );
};

export default Form;
