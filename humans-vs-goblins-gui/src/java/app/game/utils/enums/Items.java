package game.utils.enums;

import game.Environment;
import game.objects.items.*;

public enum Items {
    BRONZE_SWORD, SILVER_SWORD, GOLD_SWORD, SMALL_HEALTH_POTION, MEDIUM_HEALTH_POTION, LARGE_HEALTH_POTION;

    public Item generateItem(Environment env) {
        switch (this) {
            case BRONZE_SWORD:
                return new BronzeSword(env);
            case SILVER_SWORD:
                return new SilverSword(env);
            case GOLD_SWORD:
                return new GoldSword(env);
            case SMALL_HEALTH_POTION:
                return new SmallHealthPotion(env);
            case MEDIUM_HEALTH_POTION:
                return new MediumHealthPotion(env);
            case LARGE_HEALTH_POTION:
                return new LargeHealthPotion(env);
        }
        return null;
    }
}
