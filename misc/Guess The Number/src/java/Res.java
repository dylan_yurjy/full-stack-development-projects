public enum Res {
    WELCOME, START, VICTORY, LOW, HIGH;

    public String getRes() {
        switch (this) {
            case WELCOME:
                return "Hello! What is your name?\n";
            case START:
                return String.format("\nOkay, %s, I am thinking of a number between 1 and 20.\nTake a guess.\n", Main.name);
            case VICTORY:
                return String.format("\nGood job, %s! You guessed my number in %s guesses!\n\nWould you like to play again? (y/n)\n", Main.name, Main.guesses);
            case LOW:
                return "\nYour guess is too high.\n";
            case HIGH:
                return "\nYour guess is too low.\n";
        }
        return null;
    }
}
