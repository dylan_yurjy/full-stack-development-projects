package game.utils;

import game.objects.Environment;
import game.objects.Land;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputProcessor {

    private String input = "";
    private boolean inputAcceptable = false;
    private final Scanner scanner = new Scanner(System.in);

    public String receiveInput(Land land, Environment env) {
        while (!inputAcceptable) {
            input = scanner.nextLine();
            if (!env.isActive()) {
                return "";
            }
            if (validateInput(input)) {
                if (land.moveValidator(input)) {
                    inputAcceptable = true;
                    land.getPrinter().setHudData(13, false);
                } else {
                    land.getPrinter().setHudData(13, "Can't move there.", true);
                }
            } else {
                land.getPrinter().setHudData(13, "Invalid Input", true);
            }
        }
        inputAcceptable = false;
        return input;
    }

    public boolean validateInput(String input) {
        Pattern pattern = Pattern.compile("\\b[wdsacr](?!.+)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        return matcher.find();
    }

    public boolean gameOver() {
        System.out.println("You lost! Play again?");
        while (!input.equalsIgnoreCase("n") && !input.equalsIgnoreCase("y")) {
            System.out.println(input);
            input = scanner.nextLine();
        }
        return input.equalsIgnoreCase("y");
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

}
