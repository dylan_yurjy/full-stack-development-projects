package game.objects.items;

import game.objects.placeable.Human;
import game.utils.enums.HudComponent;
import game.utils.enums.Stat;

import java.util.HashMap;

public abstract class AbstractConsumable extends Item {

    public void activate(Human human) {
        consume(human);
    }

    {
        setConsumable(true);
    }

    public void consume(Human human) {
        HashMap<Stat, Integer> humanStatsMap = human.getStatsMap();
        getBoostMap().forEach((key, value) -> humanStatsMap.put(key, humanStatsMap.get(key) + value));
        human.getEnv().getWidgetMap().get(HudComponent.HUMAN_HEALTH_BAR).redraw();
    }


}
