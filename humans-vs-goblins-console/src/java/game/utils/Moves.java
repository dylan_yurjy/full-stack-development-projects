package game.utils;

import game.objects.Drops;
import game.objects.Environment;
import game.objects.Goblin;
import game.objects.Human;

import java.util.Arrays;

public class Moves {

    public static void executeMoves(Environment env, String input, Human human, Goblin goblin) {
        switch (input) {
            case "c" -> human.cycleWeapon(env);
            case "r" -> human.useItem(env);
            case "w", "s", "a", "d" -> {
                env.getOldLocations().addAll(Arrays.asList(new Location(human.getLocation()), new Location(goblin.getLocation())));
                switch (input) {
                    case "w" -> human.moveUp();
                    case "s" -> human.moveDown();
                    case "a" -> human.moveLeft();
                    case "d" -> human.moveRight();
                }
                int humanX = human.getLocation().getX();
                int humanY = human.getLocation().getY();
                Drops.checkForCollision(env);
                int goblinX = goblin.getLocation().getX();
                int goblinY = goblin.getLocation().getY();
                if (Combat.checkForCollision(humanX, humanY, goblinX, goblinY)) {
                    Combat.setInCombat(true);
                } else {
                    if (humanX > goblinX) {
                        goblin.moveDown();
                    } else if (humanX < goblinX) {
                        goblin.moveUp();
                    }
                    if (humanY > goblinY) {
                        goblin.moveRight();
                    } else if (humanY < goblinY) {
                        goblin.moveLeft();
                    }
                }
                if (Combat.checkForCollision(humanX, humanY, goblin.getLocation().getX(), goblin.getLocation().getY())) {
                    Combat.setInCombat(true);
                }
            }
        }
        env.getInputProcessor().setInput("");
    }
}
