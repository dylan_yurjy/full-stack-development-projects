package game.objects;

import game.exceptions.BoundsException;
import game.exceptions.OccupiedSpaceException;
import game.utils.MessagePrinter;

import java.util.ArrayList;

public class Human extends Creature {

    private final ArrayList<Item> inventory = new ArrayList<>();
    private int gold = 0;
    private final ArrayList<Item> setup = new ArrayList<>();
    private Item selected;

    {
        inventory.add(new Item("bronze sword", false, "strength", "10"));
        inventory.add(new Item("small health potion", true, "currentHealth", "20"));
        selected = inventory.get(0);
    }

    public Human(int x, int y, int str) {
        super('H', x, y, str);
    }

    @Override
    public String toString() {
        return String.valueOf(super.getRepresentation());
    }

    public void cycleWeapon(Environment env) {
        if (getSelected() != null) {
            inventory.add(selected);
            selected = inventory.remove(0);
            env.getLand().getPrinter().generateInventoryList(this);
        } else {
            env.getLand().getPrinter().setHudData(13, "No Items", true);
        }
    }

    public void useItem(Environment env) {
        if (getSelected() != null) {
            if (!getSelected().isConsumable()) {
                if (getSetup().size() == 1) {
                    getSetup().forEach( e -> e.getBoostMap().keySet().forEach( k ->  getStats().replace(k, getStats().get(k) - e.getBoostMap().get(k))));
                    getInventory().add(getSetup().remove(0));
                }
                getSetup().add(getSelected());
                getSetup().forEach( e -> e.getBoostMap().keySet().forEach( k -> getStats().replace(k, getStats().get(k) + e.getBoostMap().get(k))));
                env.getLand().getPrinter().generateWieldedList(this);
            } else {
                selected.getBoostMap().keySet().forEach(k -> getStats().replace(k, getStats().get(k) + selected.getBoostMap().get(k)));
                env.getLand().getPrinter().setHudData(0, String.format("Health: %s/%s", getStats().get("currentHealth"), getStats().get("maxHealth")), true);
            }
            try {
                setSelected(null);
                setSelected(getInventory().get(0));
            } catch (IndexOutOfBoundsException e) {
                // do nothing
            }
            env.getLand().getPrinter().generateInventoryList(this);
        } else {
            env.getLand().getPrinter().setHudData(13, "No Items", true);
        }
    }

    public static Human spawnHuman(Environment env, int x, int y) {
        MessagePrinter printer = env.getLand().getPrinter();
        Human human;
        try {
            env.getLand().validateLocation(x, y);
        } catch (BoundsException | OccupiedSpaceException e) {
            e.printStackTrace();
            return null;
        }
        human = new Human(x, y, 20);
        printer.setHudData(0, String.format("Health: %s/%s", human.getStats().get("currentHealth"), human.getStats().get("currentHealth")), true);
        printer.generateInventoryList(human);
        env.getPlacedObjects().add(human);
        return human;
    }

    public ArrayList<Item> getInventory() {
        return inventory;
    }

    public Item getSelected() {
        return selected;
    }

    public ArrayList<Item> getSetup() {
        return setup;
    }

    public int getGold() {
        return gold;
    }

    public void setSelected(Item selected) {
        this.selected = selected;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

}
