package game.objects.placeable;

import game.Environment;
import game.utils.enums.Stat;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.Canvas;

public class Goblin extends Creature {

    private static Canvas healthBar;
    private PaintListener paintListener;

    {
        paintListener = new PaintListener() {
            @Override
            public void paintControl(PaintEvent e) {
                float ratio = ((float)getStatsMap().get(Stat.CURRENT_HEALTH) / 100);
                int width = (int) (healthBar.getSize().x * ratio);
                e.gc.setBackground(getEnv().getDisplay().getSystemColor(SWT.COLOR_GREEN));
                e.gc.fillRectangle(0, 0, width, 10);
            }
        };
        healthBar.setSize(50, 10);
        healthBar.setBackground(getEnv().getDisplay().getSystemColor(SWT.COLOR_GRAY));
        getLabel().setLocation(getLocationX() * 50, getLocationY() * 50);
        healthBar.setLocation(getLabel().getLocation().x, getLabel().getLocation().y - 10);
        healthBar.addPaintListener(paintListener);
        healthBar.redraw();
    }

    public Goblin(Environment env, int x, int y) {
        super(env, x, y,env.getImageLoader().getGoblinIcon());
    }

    public Goblin(Environment env) {
        super(env, env.getImageLoader().getGoblinIcon());
    }

    public static Canvas generateGoblinHealthBar(Canvas parent) {
        return new Canvas(parent, SWT.BORDER);
    }

    @Override
    public void lowerHealth(int amount) throws SWTException {
        int currentHealth = getStatsMap().get(Stat.CURRENT_HEALTH);
        int newHealth = Math.max(currentHealth - amount, 0);
        if (newHealth == 0) {
            setAlive(false);
        }
        getStatsMap().put(Stat.CURRENT_HEALTH, newHealth);
        Environment env = getEnv();
        env.getDisplay().syncExec(() -> healthBar.redraw());
    }

    public void repositionHealthBar() {
        int x = getLabel().getLocation().x;
        int y;
        if (getLocationY() == 0) {
            y = getLabel().getLocation().y + 50;
        } else {
            y = getLabel().getLocation().y - 10;
        }
        getHealthBar().setLocation(x, y);
    }

    public static Canvas getHealthBar() {
        return Goblin.healthBar;
    }

    public static void setHealthBar(Canvas healthBar) {
        Goblin.healthBar = healthBar;
    }

    public PaintListener getPaintListener() {
        return paintListener;
    }
}
