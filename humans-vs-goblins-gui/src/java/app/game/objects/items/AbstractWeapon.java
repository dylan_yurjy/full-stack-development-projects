package game.objects.items;

import game.utils.enums.Stat;
import org.eclipse.swt.graphics.Image;

public abstract class AbstractWeapon extends AbstractEquipable {
    private final Image imageRegular;
    private final Image imageBig;

    public AbstractWeapon(int strBoost, String name, Image regular, Image big) {
        this.imageRegular = regular;
        this.imageBig = big;
        putBoostMap(Stat.STRENGTH, strBoost);
        if (getSlot().ordinal() == 0) {
            initiateCanvas( imageBig);
        } else {
            initiateCanvas(imageRegular);
        }
    }

    public Image getImageRegular() {
        return imageRegular;
    }

    public Image getImageBig() {
        return imageBig;
    }
}
