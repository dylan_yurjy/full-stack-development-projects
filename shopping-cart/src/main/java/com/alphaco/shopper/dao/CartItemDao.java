package com.alphaco.shopper.dao;

import com.alphaco.shopper.entitys.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemDao extends JpaRepository<CartItem, Integer> {

}
