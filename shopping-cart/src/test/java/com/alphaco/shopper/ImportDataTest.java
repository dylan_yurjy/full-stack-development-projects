package com.alphaco.shopper;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.*;

import com.alphaco.shopper.entitys.Product;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;


public class ImportDataTest {


    @Test
    public void testImporter() {
        byte[] arr;
        JSONObject ex;

        try {
            InputStream is = ImportDataTest.class.getResourceAsStream("/products.json");
            arr = is.readAllBytes();
            ex = new JSONObject(new String(arr));
//            System.out.println(ex);
            JSONArray array = ex.getJSONArray("products");
//            System.out.println(array.length());
//            System.out.println(array);

            List<Map<String, Object>> mapper = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                mapper.add(obj.toMap());
            }
            NumberFormat numberFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());

            mapper.forEach(e -> {
                Product product = new Product();
                for (var item: e.entrySet()) {

                    String key = item.getKey();
                    switch (key) {
                        case "price":
                            double price = Double.parseDouble( item.getValue().toString());
                            String formatted = numberFormatter.format(price);
                            product.setPrice(formatted);
                            break;
                        case "title":
                            product.setTitle(item.getValue().toString());
                            break;
                        case "description":
                            product.setDescription(item.getValue().toString());
                            break;
                        case "stock":
                            product.setStock(Integer.parseInt(item.getValue().toString()));
                            break;
                        case "rating":
                            product.setRating(Float.parseFloat(item.getValue().toString()));
                            break;
                        case "brand":
                            product.setBrand(item.getValue().toString());
                            break;
                        case "category":
                            product.setCategory(item.getValue().toString());
                            break;
                        case "thumbnail":
                            product.setThumbnail_url(item.getValue().toString());
                            break;
                        case "images":
                            product.setImages((ArrayList<String>) item.getValue());
                            break;

                    }
                }
                System.out.println(product);
            });



//            for (var item : array.toList()) {
//                System.out.println(item);
//                JSONObject obj = array.
//
//            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
