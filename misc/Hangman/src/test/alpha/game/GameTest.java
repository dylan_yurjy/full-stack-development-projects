package alpha.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private Game game;

    @BeforeEach
    void beforeEach() {
        game = new Game(true);
        Config.setWords("development".toCharArray());
        game.devSetInput("p");
        game.processInput();
        game.devSetInput("z");
        game.processInput();
        game.devSetInput("d");
        game.processInput();
    }

    @ParameterizedTest
    @ValueSource(strings = {"a", "B", "G", "L", "a", "b", "Q", "five"})
    void checkErroneousSingle(String word) {
        game.devSetInput(word);
        assertFalse(game.checkErroneous(), "Correct Input Has Been Flagged!");
    }

    @ParameterizedTest
    @ValueSource(strings = {"Hello!", "World!", "Lovely!", "GenSpark!"})
    void checkErroneousMulti(String word) {
        game.devSetInput(word);
        assertFalse(game.checkErroneous(), "Correct Input Has Been Flagged!");
    }

    @ParameterizedTest
    @ValueSource(strings = {"1111111", "22222222", "    ", "FIVE", "Hello", "b"})
    void checkErroneous(String word) {
        game.devSetInput(word);
        assertTrue(game.checkErroneous(), "Incorrect Input Was Accepted!");
    }

    @ParameterizedTest
    @ValueSource(strings = {"R", "b", "c", "L", "e", "f", "a", "g", "O"})
    void processInputGuess(String word) {
        game.devSetInput(word);
        game.processInput();
        assertTrue(Config.getGuessMap().get("totalGuesses").contains(word.toLowerCase()), "Guess Was Not Added To GuessMap");
    }

    @ParameterizedTest
    @ValueSource(strings = {"development!", "DEVELOPMENT!", "DeVeLoPMeNT!"})
    void processInputBanger(String word) {
        game.devSetInput(word);
        game.processInput();
        game.update();
        assertTrue(Config.getHasWon(), "Banger was not recognized");
    }

    @ParameterizedTest
    @ValueSource(strings = {"wrong!", "error!", "SeRiOuS!"})
    void processInputBangerFailure(String word) {
        game.devSetInput(word);
        game.processInput();
        game.update();
        assertFalse(Config.isProgressing(), "Incorrect Banger did not result in failure.");
    }

    @ParameterizedTest
    @ValueSource(strings = {"p", "Z", "D"})
    void processInputDuplicate(String word) {
        game.devSetInput(word);
        game.processInput();
        game.devSetInput(word);
        game.processInput();
        game.devSetInput(word);
        game.processInput();
        game.update();
        assertTrue(Config.getGuessMap().get("totalGuesses").contains(word.toLowerCase()), "Duplicate was not recognized");
    }

    @Test
    void processDefeat() {
        game.devSetInput("z");
        game.processInput();
        game.devSetInput("a");
        game.processInput();
        game.devSetInput("b");
        game.processInput();
        game.devSetInput("r");
        game.processInput();
        game.devSetInput("q");
        game.processInput();
        game.devSetInput("i");
        game.processInput();
        game.devSetInput("m");
        game.processInput();
        game.devSetInput("y");
        game.processInput();
        game.update();
        Config.setIsProgressing(true);
        game.finish();
        assertFalse(Config.getHasWon(), "Simulated Failure Failed");
    }
}