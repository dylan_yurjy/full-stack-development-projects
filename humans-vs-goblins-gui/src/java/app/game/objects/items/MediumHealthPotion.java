package game.objects.items;

import game.Environment;

public class MediumHealthPotion extends AbstractHealthPotion {
    private final String name = "Medium Health Potion";

    public MediumHealthPotion(Environment env) {
        super(30);
        initiateCanvas(env.getImageLoader().getMediumHealthPotionIcon());
    }
}
