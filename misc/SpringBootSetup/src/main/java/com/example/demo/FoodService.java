package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FoodService {

    @Autowired
    FoodDao foodDao;

    public void addItem(String name) {
        Food food = new Food();
        food.setName("strawberry");
        foodDao.save(food);
    }
}
