package com.alphaco.shopper.gui;

import com.alphaco.shopper.entitys.CartItem;
import com.alphaco.shopper.entitys.Product;

import javax.swing.*;
import javax.swing.border.SoftBevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import static com.alphaco.shopper.gui.Swing.*;
import static com.alphaco.shopper.gui.Utils.*;

public class ProductCard extends JPanel {
    private final CartPanel cartPanel;
    private final JLabel stockLabel;
    private final Product product;
    private int index;

    public ProductCard(CartPanel cartPanel, Product product, int numCols, int index, int scrollPaneWidth) throws IOException {
        this.cartPanel = cartPanel;
        this.product = product;
        this.index = index;
        stockLabel = new JLabel();
        initialize(numCols, scrollPaneWidth);
    }

    private void initialize(int numCols, int scrollPaneWidth) {
        int colNum = index % numCols == 0 ? numCols : index % numCols;
        int rowNum = (int) Math.ceil((float) index / numCols);

        int spacing = (scrollPaneWidth - (numCols * 400)) / ((numCols) + 1);

        int locationX = ((colNum - 1) * 400) + (colNum * spacing);
        int locationY = ((rowNum - 1) * 200) + (rowNum * 20);

        this.setSize(400, 200);
        this.setLocation(locationX, locationY);
        this.setLayout(null);

        ImageIcon imageIcon = getProductIcon(product.getThumbnail_url());
        JLabel picture = new JLabel(imageIcon);
        JLabel priceLabel = new JLabel(product.getPrice());
        JLabel titleLabel = new JLabel(product.getTitle());
        JLabel brandLabel = new JLabel(product.getBrand());
        JTextArea descLabel = new JTextArea();
        JLabel ratingLabel = new JLabel(String.format(
                "Rating: %s/5",
                product.getRating()));
        JButton addToCartButton = new JButton("Add to Cart");

        picture.setSize(150, 150);
        picture.setLocation(10, 10);

        priceLabel.setFont(titleFont);
        priceLabel.setSize(priceLabel.getPreferredSize().width, priceLabel.getPreferredSize().height);
        priceLabel.setLocation((150 - priceLabel.getWidth()) / 2 + 10, (40 - priceLabel.getHeight()) / 2 + 160);

        titleLabel.setFont(titleFont);
        titleLabel.setSize(titleLabel.getPreferredSize());
        titleLabel.setLocation((240 - titleLabel.getWidth()) / 2 + 160, 10);

        brandLabel.setFont(brandFont);
        brandLabel.setSize(brandLabel.getPreferredSize());
        brandLabel.setLocation((240 - brandLabel.getWidth()) / 2 + 160, 30);

        descLabel.setText(product.getDescription());
        descLabel.setFont(descFont);
        descLabel.setSize(200, 80);
        descLabel.setLineWrap(true);
        descLabel.setWrapStyleWord(true);
        descLabel.setFocusable(false);
        descLabel.setEditable(false);
        descLabel.setBorder(UIManager.getBorder("Label.border"));
        descLabel.setBackground(UIManager.getColor("Label.background"));
        descLabel.setLocation((240 - descLabel.getWidth()) / 2 + 160, 55);

        ratingLabel.setFont(infoFont);
        ratingLabel.setSize(90, 50);
        ratingLabel.setLocation(160 + 30, 125);

        stockLabel.setText(String.format("Stock: %s", product.getStock()));
        stockLabel.setFont(infoFont);
        stockLabel.setSize(60, 50);
        stockLabel.setLocation(400 - 105, 125);

        addToCartButton.setSize(100, 20);
        addToCartButton.setLocation( 160 + (240 - addToCartButton.getWidth()) / 2, 170);
        addToCartButton.setFocusable(false);

        addToCartListener(addToCartButton);

        this.add(addToCartButton);
        this.add(stockLabel);
        this.add(ratingLabel);
        this.add(descLabel);
        this.add(brandLabel);
        this.add(titleLabel);
        this.add(priceLabel);
        this.add(picture);
        this.setBorder(BorderFactory.createSoftBevelBorder(SoftBevelBorder.RAISED));
    }

    private void addToCartListener(JButton addToCartButton) {
        addToCartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean contains = false;
                if (cartPanel.getCartItems() != null) {
                    for (var item : cartPanel.getCartItems()) {
                        CartItem cartItem = (CartItem) item;
                        if (cartItem.getId() == product.getId()) {
                            contains = true;
                            break;
                        }
                    }
                }
                if (contains) {
                    JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), "Item already in cart");
                } else {
                    List<?> cartList = addToCart(product.getId());
                    if (cartList != null) {
                        cartPanel.refreshCart(cartList);
                        int curStock = product.getStock();
                        product.setStock(curStock - 1);
                        stockLabel.setText(String.format("Stock: %s", curStock - 1));
                    }
                }
            }
        });
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int i) {
        this.index = i;
    }

    public Product getProduct() {
        return product;
    }

    public JLabel getStockLabel() {
        return stockLabel;
    }
}
