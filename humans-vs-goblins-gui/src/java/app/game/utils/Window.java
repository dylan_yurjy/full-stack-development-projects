package game.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.DeviceData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Window {

    private final Display display;
    private final Shell shell;

    public Window() {
        this.display = new Display();
        this.shell = new Shell(display, SWT.SHELL_TRIM & (~SWT.RESIZE));
        Rectangle trimmed = shell.computeTrim(0, 0, 755, 855);
        shell.setSize(trimmed.width, trimmed.height);
        shell.setText("Humans vs. Goblins");
        shell.layout();
        shell.open();
    }

    public Display getDisplay() {
        return display;
    }

    public Shell getShell() {
        return shell;
    }

}
