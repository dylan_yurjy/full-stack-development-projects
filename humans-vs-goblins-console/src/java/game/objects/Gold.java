package game.objects;

import java.util.Random;

public class Gold extends Placeable {

    private final int amount;

    {
        Random rand = new Random();
        amount = rand.nextInt(1, 101);
    }

    public Gold(int x, int y) {
        super('M', x, y);
    }

    @Override
    public String toString() {
        return String.valueOf(super.getRepresentation());
    }

    public static void spawnTreasure(Environment env) {
        Random rand = new Random();
        int x = rand.nextInt(1, 14);
        int y = rand.nextInt(2, 28);
        while (env.getLand().getLand(x, y) != ' ') {
            x = rand.nextInt(1, 14);
            y = rand.nextInt(2, 28);
        }
        Gold gold = new Gold(x, y);
        env.getPlacedObjects().add(gold);
    }

    public int getAmount() {
        return amount;
    }
}

