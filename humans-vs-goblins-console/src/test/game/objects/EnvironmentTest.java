package game.objects;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class EnvironmentTest {

    Environment env = new Environment(true);
    Random rand = new Random();

    @RepeatedTest(value = 20, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Spawn Tester")
    void testHGSpawnTester() {
        int x = 0;
        while (x < 1) {
            x = rand.nextInt(14);
        }
        int y = 0;
        while (y < 2) {
            y = rand.nextInt(28);
        }
        if ((x + y) % 2 == 0) {
            Human human = Human.spawnHuman(env, x, y);
            env.updateLand();
            assertTrue(env.getPlacedObjects().contains(human), "Human was not spawned correctly.");

        } else {
            Goblin goblin = Goblin.spawnGoblin(env, x, y);
            env.updateLand();
            assertTrue(env.getPlacedObjects().contains(goblin), "Goblin was not spawned correctly.");
        }
    }

    @RepeatedTest(value = 20, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Repeated Location Test")
    void testLocationValidator() {
        int x = 0;
        while (x < 1) {
            x = rand.nextInt(14);
        }
        int y = 0;
        while (y < 2) {
            y = rand.nextInt(28);
        }
        final int a = x;
        final int c = y;
        assertDoesNotThrow(() -> env.getLand().validateLocation(a, c), "Validation Failed");
    }
}