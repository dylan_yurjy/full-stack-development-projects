package game.objects;

import java.util.ArrayList;

public class Drops extends Placeable {

    public static ArrayList<Item> tier1Drop = new ArrayList<>();
    public static ArrayList<Item> tier2Drop = new ArrayList<>();
    private ArrayList<Item> thisDrop;

     static {
        tier1Drop.add(new Item("silver sword", false, "strength", "20"));
        tier1Drop.add( new Item("medium potion", true, "currentHealth", "40"));
        tier2Drop.add(new Item("gold sword", false, "strength", "50"));
        tier2Drop.add(new Item("large potion", true, "currentHealth", "50"));
    }

    public Drops(char rep, int x, int y, int tier) {
         super(rep, x, y);
         if (tier == 1) {
             thisDrop = new ArrayList<>(tier1Drop);
         } else if (tier >= 2) {
             thisDrop = new ArrayList<>(tier2Drop);
         }
    }

    @Override
    public String toString() {
        return String.valueOf(super.getRepresentation());
    }

    public static void checkForCollision(Environment env) {
         Human human = env.getHuman();
         ArrayList<Placeable> removeDrops = new ArrayList<>();
         for (var object : env.getPlacedObjects()) {
             if (object instanceof Drops) {
                 if (object.getLocation().equals(human.getLocation())) {
                     human.getInventory().addAll(((Drops) object).getThisDrop());
                     removeDrops.add(object);
                     env.getOldLocations().add(object.getLocation());
                 }
             } else if (object instanceof Gold) {
                 if (object.getLocation().equals(human.getLocation())) {
                     human.setGold(human.getGold() + ((Gold) object).getAmount());
                     removeDrops.add(object);
                     env.getOldLocations().add(object.getLocation());
                     env.getLand().getPrinter().updateGold(human);
                 }
             }
         }
         env.getLand().getPrinter().generateInventoryList(human);
         if (removeDrops.size() > 0) {
             for (var object : removeDrops) {
                 env.getPlacedObjects().remove(object);
             }
         }
    }

    public ArrayList<Item> getThisDrop() {
        return thisDrop;
    }
}
