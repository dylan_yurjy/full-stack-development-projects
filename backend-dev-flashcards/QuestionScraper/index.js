import cheerio from 'cheerio'
import fetch from 'node-fetch'
import fs from 'fs'



const getRawData = (URL) => {
    return fetch(URL)
        .then(res => res.text())
        .then(data => data)
}


let urls = JSON.parse(fs.readFileSync("./data.json", 'utf-8'));

let questionArray = [];
let questionData = {};
let questionHTML = cheerio.load('<div></div>');

console.log(urls)

for (let idx = 0; idx < urls.length; idx++) {
    let data = await getRawData(urls[idx]['path'])

    let loadedURL = cheerio.load(data);

    loadedURL('script').remove()

    let questions = loadedURL('#city').find('td').first();

    questions.find('a').each((i, e) => {
        delete e.attribs.href;
    })
    questions.children().each( (i, e) => {
        let withinHr = true;

        if (e.name === 'hr') {
            questionData['html'] = questionHTML('div').first().toString();
            questionData['category'] = urls[idx]['category']
            questionData['subCategory'] = urls[idx]['subCategory']
            console.log(questionData)
            questionArray.push(questionData);
            withinHr = false;
            questionData = {}
            questionHTML = cheerio.load('<div> </div>')
        }

        if (withinHr) {
            if (e.name === 'a') {
                // skip
            } else if (e.attribs['class'] === 'h3') {
                questionData['question'] = e['children'][0]['data'];
            } else {
                questionHTML('div').first().append(e);
            }
        }
    })

    fs.writeFileSync('./questions.json', JSON.stringify(questionArray));
}


