package com.alphaco.shopper.dao;

import com.alphaco.shopper.entitys.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    @Query("Select u FROM User u WHERE u.name =:name")
    public User findByName(@Param("name") String name);
}
