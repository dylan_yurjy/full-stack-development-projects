package com.alphaco.shopper;

import com.alphaco.shopper.gui.Swing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args ) throws IOException, InterruptedException, URISyntaxException, ClassNotFoundException {
        Swing swing = new Swing();
        ApplicationContext applicationContext = SpringApplication.run(App.class, args);
        swing.start();
    }
}
