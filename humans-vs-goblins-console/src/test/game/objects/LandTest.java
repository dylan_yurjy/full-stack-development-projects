package game.objects;
import game.utils.Location;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class LandTest {

    Environment env = new Environment(true);
    Random rand = new Random();


    @ParameterizedTest()
    @ValueSource(chars = {'a', '%', '5', ')', '(', 'S', 'X', '@'})
    @DisplayName("Land Manipulation Tester")
    void testLocationValidator(char letter) {
        int x = 0;
        while (x < 1) {
            x = rand.nextInt(14);
        }
        int y = 0;
        while (y < 2) {
            y = rand.nextInt(28);
        }
        final int a = x;
        final int c = y;
        assertTrue(env.getLand().editLand(letter, a, c), "Land was not edited correctly");
        env.getLand().renderLand();
    }

    @RepeatedTest(value = 20, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Land Manipulation Validation Tester")
    void editLandCordValidator() {
        int x = 0;
        while (x < 1) {
            x = rand.nextInt(14);
        }
        int y = 0;
        while (y < 2) {
            y = rand.nextInt(28);
        }
        final int a = x;
        final int c = y;
        assertDoesNotThrow(() -> env.getLand().validateManipulation(a, c), "Validation Failed");
    }

    @RepeatedTest(value = 80, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Placeable Move Validation Tester")
    void placeableMoveValidationTest() {
        int x = rand.nextInt(4);
        String a = "";
        switch (x) {
            case 0 -> {
                a = "w";
                env.setHuman(Human.spawnHuman(env, 1, 5));
            }
            case 1 -> {
                a = "s";
                env.setHuman(Human.spawnHuman(env, 13, 6));

            }
            case 2 -> {
                a = "d";
                env.setHuman(Human.spawnHuman(env, 1, 27));

            }
            case 3 -> {
                a = "a";
                env.setHuman(Human.spawnHuman(env, 1, 2));

            }
        }
        env.updateLand();
        assertFalse(env.getLand().moveValidator(a));
    }
}