package alpha.game;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {


    private String input;
    private boolean inputAcceptable;
    private static final Scanner scanner = new Scanner(System.in);

    public Game() {
        start();
    }

    public Game(boolean devMode) {
        if (devMode) {
            input = "";
            Config.initializeSettings();
        }
    }

    public void start() {
        input = "";
        inputAcceptable = false;
        Config.initializeSettings();
        update();
        mainLoop();
    }

    public void mainLoop() {
        while (Config.isProgressing()) {
            System.out.println("Guess A Letter! ");
            receiveInput();
            processInput();
            update();
        }
        finish();
    }

    public void finish() {
        if (checkVictorious()) {
            victory();
        } else {
            defeat();
        }
        playAgain();
    }

    public void receiveInput() {
        while (!inputAcceptable) {
            input = scanner.nextLine();
            if (checkErroneous()) {
                System.out.println("Try Again");
            } else {
                inputAcceptable = true;
            }
        }
        inputAcceptable = false;
    }

    public void devSetInput(String word) {
        input = word;
    }


    public boolean checkErroneous() {
        input = input.toLowerCase();
        int l = input.length();
        if (l == 1) {
            Pattern incorrectIdentifier = Pattern.compile("[^a-z]", Pattern.CASE_INSENSITIVE);
            Matcher matcher = incorrectIdentifier.matcher(input);
            return matcher.find();
        } else if (l > 1 && input.contains("!")) {
            Pattern incorrectIdentifier = Pattern.compile("[^a-z]!$", Pattern.CASE_INSENSITIVE);
            Matcher matcher = incorrectIdentifier.matcher(input);
            return matcher.find();
        } else {
            return true;
        }
    }

    public void processInput() {
        input = input.toLowerCase();
        if (checkBanger()) {
            Config.updatePlayerWord(Config.getWord().toCharArray());
        } else if (checkDuplicate()) {
            duplicateGuess();
        } else if (checkGuess()) {
            correctGuess();
        } else {
            incorrectGuess();
        }
    }

    public boolean checkGuess() {
        for (var letter : Config.getWord().toCharArray()) {
            if (input.equalsIgnoreCase(String.valueOf(letter))) {
                return true;
            }
        }
        return false;
    }

    public boolean checkBanger() {
        int l = input.length();
        for (var letter : input.toCharArray()) {
            if (String.valueOf(letter).equals("!")) {
                Config.setIsProgressing(false);
                return input.substring(0, l - 1).equalsIgnoreCase(Config.getWord());
            }
        }
        return false;
    }

    public boolean checkDuplicate() {
        for (int i = 0; i < Config.guessMap.get("totalGuesses").size(); i++) {
            if (input.equalsIgnoreCase(String.valueOf(Config.guessMap.get("totalGuesses").get(i)))) {
                return true;
            }
        }
        return false;
    }

    public void duplicateGuess() {
        System.out.println("You already guessed '" + input.toUpperCase() + "'");
    }

    public void incorrectGuess() {
        Config.updateGuessMap("incorrect", input);
        Config.setCurrentStage(Config.getCurrentStage() + 1);
    }

    public void correctGuess() {
        for (int i = 0; i < Config.getWord().length(); i++) {
            if (input.equals(String.valueOf(Config.getWord().toCharArray()[i]))) {
                Config.updatePlayerWord(i, input);
            }
        }
        Config.updateGuessMap("correct", input);
    }

    public boolean checkVictorious() {
        return Config.getHasWon();
    }

    public void victory() {
        System.out.println("Congratulations! You guessed the word! :)");
    }

    public void defeat() {
        System.out.println("Sorry! You Lost :(");
    }

    public void playAgain() {
        System.out.println("Play again? y/n");
        while (!Config.isProgressing()) {
            input = scanner.nextLine();
            if (input.equals("y")) {
                start();
            } else if (input.equals("n")) {
                System.out.println("Thanks for playing!");
                Config.setIsProgressing(false);
                Config.setIsPlaying(false);
                scanner.close();
            } else {
                System.out.println("Incorrect Input");
            }
        }
    }

    public void update() {
        switch (Config.getCurrentStage()) {
            case 0 -> Pics.NEW.getPic();
            case 1 -> Pics.HEAD.getPic();
            case 2 -> Pics.BODY1.getPic();
            case 3 -> Pics.BODY2.getPic();
            case 4 -> Pics.ARM1.getPic();
            case 5 -> Pics.ARM2.getPic();
            case 6 -> Pics.LEG1.getPic();
            case 7 -> {
                Pics.LEG2.getPic();
                Config.setIsProgressing(false);
                Config.setHasWon(false);
                return;
            }
        }
        if (Config.getPlayerWord().equalsIgnoreCase(Config.getWord())) {
            Config.setIsProgressing(false);
            Config.setHasWon(true);
        }
    }
}
