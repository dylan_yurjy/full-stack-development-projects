package com.alphaco.shopper.entitys;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tbl_cart")
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private float totalPrice;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @Basic( fetch = FetchType.EAGER)
    private List<CartItem> productList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<CartItem> getProducts() {
        return productList;
    }

    public void setProducts(List<CartItem> products) {
        this.productList = products;
    }

    public void addToPrice(float amount) {
        this.totalPrice += amount;
    }

    public void subtractFromPrice(float amount) {
        this.totalPrice -= amount;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", totalPrice=" + totalPrice +
                ", products=" + productList +
                '}';
    }
}
