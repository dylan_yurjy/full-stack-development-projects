package com.alphaco.objects;

import java.util.List;

public class Student {
    private int id;
    private String name;
    private List<Phone> phone;
    private Address address;

    public Student(int id, String name, List<Phone> phone, Address address) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone=" + phone +
                ", address=" + address +
                '}';
    }
}
