package game.utils;

import game.Environment;
import org.eclipse.swt.SWTException;

import static game.Environment.*;

public class Combat {
    private static Environment env;
    private static volatile boolean humanInCombat = false;
    private static volatile boolean threadRunning = false;

    public static void initializeVariables(Environment env) {
        Combat.env = env;
    }

    public static Thread initiateBattleThread() {
        return new Thread(() -> {
            try {
                while (env.getLand().checkCollided()) {
                    human.attack(goblin);
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!human.isAlive()) {
                        break;
                    }
                }
                setHumanInCombat(false);
            } catch (SWTException e) {
                if (e.getMessage().contains("disposed")) {
                    System.out.println("Exception contains disposed --- DY" );
                } else {
                    System.out.println("Different Exception --- DY");
                }
            }

        });
    }

    public static void initiateCombat() {
        if (!threadRunning) {
            setThreadRunning(true);
            initiateBattleThread().start();
            setThreadRunning(false);
        }
    }

    public static void setThreadRunning(boolean threadRunning) {
        Combat.threadRunning = threadRunning;
    }

    public static boolean isHumanInCombat() {
        return humanInCombat;
    }

    public static void setHumanInCombat(boolean humanInCombat) {
        Combat.humanInCombat = humanInCombat;
    }
}
