package alpha.game;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class WordLoader {

    static String secretWord() {
        byte[] arr;
        JSONObject ex;
        JSONArray json;
        Random rand = new Random();
        int index = rand.nextInt(0, 2466);

        try {
            InputStream is = Main.class.getResourceAsStream("/words.json");
            arr = is.readAllBytes();
            ex = new JSONObject(new String(arr));
            json = new JSONArray(ex.getJSONArray("data"));
            return json.toList().get(index).toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Error: Unable to import JSON file.";
        } catch (NullPointerException e1) {
            e1.printStackTrace();
            return "Error: Unable to read byte data successfully.";
        }
    }

}
