package game;

import game.ai.GoblinAI;
import game.objects.items.BronzeSword;
import game.objects.placeable.*;
import game.utils.Combat;
import game.utils.ImageLoader;
import game.utils.KeyListeners;
import game.utils.Window;
import game.utils.enums.HudComponent;
import game.utils.enums.Stat;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

public class Environment {
    private final Window window;
    private final Display display;
    private final Shell shell;
    private final Land land;
    private final ImageLoader imageLoader;
    private final HashMap<HudComponent, Composite> widgetMap = new HashMap<>();
    private final ArrayList<GameObject> gameObjects = new ArrayList<>();
    private final ArrayList<Point> oldLocations = new ArrayList<>();
    public static Human human;
    public static Goblin goblin;
    public static Environment env;
    private Treasure treasure;

    public Environment() {
        Environment.env = this;
        this.window = new Window();
        this.display = window.getDisplay();
        this.shell = window.getShell();
        this.imageLoader = new ImageLoader(display);
        widgetMap.put(HudComponent.LAND_CANVAS, generateLandCanvas());
        widgetMap.put(HudComponent.HUD_CANVAS, generateHudCanvas());
        widgetMap.put(HudComponent.HUMAN_HEALTH_BAR, generateHumanHealthBar());
        widgetMap.put(HudComponent.COINS_CANVAS, generateCoinsCanvas());
        widgetMap.put(HudComponent.SKULLS_CANVAS, generateSkullsCanvas());
        widgetMap.put(HudComponent.INVENTORY_CANVAS, generateInventoryCanvas());
        Goblin.setHealthBar(Goblin.generateGoblinHealthBar((Canvas) widgetMap.get(HudComponent.LAND_CANVAS)));
        this.land = new Land(this);
        human = new Human(this, 0, 0);
        human.getItems().add(new BronzeSword(this));
        human.getItems().get(0).activate(human);
        goblin = new Goblin(this, 14, 14);
        this.treasure = new Treasure(this);
        this.gameObjects.add(human);
        this.gameObjects.add(goblin);
        this.gameObjects.add(treasure);
        land.updateLand();
        KeyListeners.addKeyListeners();
        GoblinAI.initializeVariables(this);
        Combat.initializeVariables(this);
        GoblinAI.initiateAI();
        swtLoop();
    }

    private Canvas generateLandCanvas() {
        Canvas canvas = new Canvas(shell, SWT.BORDER);
        canvas.setBackground(display.getSystemColor(SWT.COLOR_DARK_GREEN));
        canvas.setSize(755,754);
        return canvas;
    }

    private Canvas generateHudCanvas() {
        Canvas canvas = new Canvas(shell, SWT.BORDER);
        canvas.setBackground(display.getSystemColor(SWT.COLOR_DARK_CYAN));
        canvas.setSize(755, 101);
        canvas.setLocation(0, 754);
        return canvas;
    }

    private Canvas generateHumanHealthBar() {
        Canvas humanHealthBar = new Canvas(widgetMap.get(HudComponent.HUD_CANVAS), SWT.BORDER);
        humanHealthBar.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
        humanHealthBar.setSize(500, 10);
        humanHealthBar.setLocation((humanHealthBar.getParent().getClientArea().width - humanHealthBar.getSize().x) / 2, 5);

        PaintListener healthPaintListener = e -> {
            float ratio = ((float) human.getStatsMap().get(Stat.CURRENT_HEALTH) / 100);
            int width = (int) (500 * ratio);
            e.gc.setBackground(display.getSystemColor(SWT.COLOR_RED));
            e.gc.fillRectangle(0, 0, width, 10);
        };

        humanHealthBar.addPaintListener(healthPaintListener);
        return humanHealthBar;
    }

    private Canvas generateCoinsCanvas() {
        Canvas coinsCanvas = new Canvas(widgetMap.get(HudComponent.HUD_CANVAS),SWT.NONE);
        coinsCanvas.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        coinsCanvas.setSize(50, 100);
        coinsCanvas.setLocation((widgetMap.get(HudComponent.HUMAN_HEALTH_BAR).getLocation().x - coinsCanvas.getSize().x) / 2, 10);

        Label coinsLabel = new Label(coinsCanvas, SWT.CENTER);
        coinsLabel.setImage(imageLoader.getCoinsIcon());
        coinsLabel.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        coinsLabel.setSize(50, 58);

        Label coinsAmount = new Label(coinsCanvas, SWT.CENTER);
        coinsAmount.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        FontData fD = new FontData("Vivian", 16, SWT.NONE);
        coinsAmount.setForeground(new Color(display, 242, 207, 54));
        coinsAmount.setFont(new Font(display, fD));
        coinsAmount.setText("");
        coinsAmount.setLocation(0, 60);
        coinsAmount.setSize(50, 42);

        coinsAmount.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                coinsAmount.getFont().dispose();
            }
        });

        return coinsCanvas;
    }

    private Canvas generateSkullsCanvas() {
        Canvas skullsCanvas = new Canvas(widgetMap.get(HudComponent.HUD_CANVAS),SWT.NONE);
        Composite reference = widgetMap.get(HudComponent.HUMAN_HEALTH_BAR);
        int labelWidth = 50;
        int space = reference.getParent().getClientArea().width - (reference.getLocation().x + reference.getClientArea().width);
        int x = (reference.getLocation().x + reference.getSize().x) + ((space - labelWidth) / 2);
        skullsCanvas.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        skullsCanvas.setSize(50, 90);
        skullsCanvas.setLocation(x, 10);

        Label skullsLabel = new Label(skullsCanvas, SWT.CENTER);
        skullsLabel.setImage(imageLoader.getSkullsIcon());
        skullsLabel.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        skullsLabel.setSize(50, 55);

        Label skullsAmount = new Label(skullsCanvas, SWT.CENTER);
        skullsAmount.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        FontData fD = new FontData("Vivian", 16, SWT.NONE);
        skullsAmount.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
        skullsAmount.setFont(new Font(display, fD));
        skullsAmount.setText("0");
        skullsAmount.setLocation(0, 57);
        skullsAmount.setSize(50, 25);

        skullsAmount.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                skullsAmount.getFont().dispose();
            }
        });

        return skullsCanvas;
    }

    public Canvas generateInventoryCanvas() {
        Canvas canvas = new Canvas(widgetMap.get(HudComponent.HUD_CANVAS), SWT.NONE);
        canvas.setBackground(display.getSystemColor(SWT.COLOR_TRANSPARENT));
        canvas.setSize(430, 70);
        canvas.setLocation((canvas.getParent().getClientArea().width - canvas.getSize().x) / 2, ((canvas.getParent().getClientArea().height - canvas.getSize().y) / 2) + 7);
        return canvas;
    }

    public void updateCoinsLabel() {
        Label label = (Label) getWidgetMap().get(HudComponent.COINS_CANVAS).getChildren()[1];
        label.setText(String.valueOf(human.getGold()));
    }

    public void updateSkullsLabel() {
        Label label = (Label) getWidgetMap().get(HudComponent.SKULLS_CANVAS).getChildren()[1];
        label.setText(String.valueOf(human.getSkulls()));
    }

    public void gameOver() {
        display.syncExec( () -> {
            Shell dialogShell = new Shell(display, SWT.SHELL_TRIM & (~SWT.RESIZE));
            Rectangle trimmed = dialogShell.computeTrim(0, 0, 400, 105);
            dialogShell.setSize(trimmed.width, trimmed.height);
            {
                shell.setEnabled(false);
                KeyListeners.removeKeyListeners();
                int shellX = shell.getLocation().x;
                int shellY = shell.getLocation().y;
                dialogShell.setLocation(shellX + ((shell.getClientArea().width - dialogShell.getClientArea().width) / 2), shellY + ((shell.getClientArea().height - dialogShell.getClientArea().height) / 2));
            }
            Label textLabel = new Label(dialogShell, SWT.CENTER);
            textLabel.setText(String.format("You died! You collected a total of %s goblin skulls\r\n\r\nWould you like to play again?", human.getSkulls()));
            textLabel.setSize(300,50);
            textLabel.setLocation((dialogShell.getClientArea().width - textLabel.getSize().x) / 2, 10);
            int buttonHeight = 20;
            int buttonWidth = 80;
            int parentWidth = dialogShell.getClientArea().width;
            int combinedWidth = (buttonWidth*2) + 50;
            int yCord = 75;
            Button yesButton = new Button(dialogShell, SWT.PUSH);
            Button noButton = new Button(dialogShell, SWT.PUSH);
            yesButton.setSize(buttonWidth, buttonHeight);
            yesButton.setText("Yes");
            yesButton.setLocation((parentWidth - combinedWidth) / 2, yCord);
            yesButton.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    dialogShell.dispose();
                    restart();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {

                }
            });
            noButton.setSize(buttonWidth, buttonHeight);
            noButton.setText("No");
            noButton.setLocation((noButton.getLocation().x + combinedWidth), yCord);
            noButton.addSelectionListener(new SelectionListener() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    dialogShell.dispose();
                    shell.dispose();
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {

                }
            });

            dialogShell.layout();
            dialogShell.open();
        });
    }

    public void restart() {
        shell.setEnabled(true);
        shell.setFocus();
        gameObjects.clear();
        widgetMap.forEach((k, v) -> {
            v.dispose();
        });
        widgetMap.put(HudComponent.LAND_CANVAS, generateLandCanvas());
        widgetMap.put(HudComponent.HUD_CANVAS, generateHudCanvas());
        widgetMap.put(HudComponent.HUMAN_HEALTH_BAR, generateHumanHealthBar());
        widgetMap.put(HudComponent.COINS_CANVAS, generateCoinsCanvas());
        widgetMap.put(HudComponent.SKULLS_CANVAS, generateSkullsCanvas());
        widgetMap.put(HudComponent.INVENTORY_CANVAS, generateInventoryCanvas());
        Goblin.setHealthBar(Goblin.generateGoblinHealthBar((Canvas) widgetMap.get(HudComponent.LAND_CANVAS)));
        Environment.human = new Human(this, 0, 0);
        Environment.goblin = new Goblin(this, 14, 14);
        this.treasure = new Treasure(this);
        human.getItems().add(new BronzeSword(this));
        human.getItems().get(0).activate(human);
        gameObjects.add(human);
        gameObjects.add(goblin);
        gameObjects.add(treasure);
        land.updateLand();
        GoblinAI.initiateAI();
        KeyListeners.addKeyListeners();
    }

    public void swtLoop() {
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        try {
            display.dispose();
        } catch (SWTException e) {
            if (e.getMessage().contains("disposed")) {
                System.exit(0);
            } else {
                System.exit(99999999);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        new Environment();
    }

    public Display getDisplay() {
        return display;
    }

    public Land getLand() {
        return land;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public ArrayList<GameObject> getGameObjects() {
        return gameObjects;
    }

    public ArrayList<Point> getOldLocations() {
        return oldLocations;
    }

    public HashMap<HudComponent, Composite> getWidgetMap() {
        return widgetMap;
    }

    public void setTreasure(Treasure treasure) {
        this.treasure = treasure;
    }

    public Treasure getTreasure() {
        return treasure;
    }

    public Shell getShell() {
        return this.shell;
    }
}
