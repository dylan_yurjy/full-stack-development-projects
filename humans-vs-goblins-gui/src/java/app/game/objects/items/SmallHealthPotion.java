package game.objects.items;

import game.Environment;

public class SmallHealthPotion extends AbstractHealthPotion {
    private final String name = "Small Health Potion";

    public SmallHealthPotion(Environment env) {
        super(15);
        initiateCanvas(env.getImageLoader().getSmallHealthPotionIcon());
    }
}
