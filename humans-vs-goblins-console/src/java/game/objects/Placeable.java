package game.objects;
import game.utils.Location;

public class Placeable {

    public Location location;
    private final char representation;

    public Placeable(char rep, int x, int y) {
        representation = rep;
        location = new Location(x, y);
    }

    public void moveUp() {
        location.setX(location.getX() - 1);
    }

    public void moveDown() {
        location.setX(location.getX() + 1);
    }

    public void moveLeft() {
        location.setY(location.getY() - 1);
    }

    public void moveRight() {
        location.setY(location.getY() + 1);
    }

    public Location getLocation() {
        return location;
    }

    public char getRepresentation() {
        return representation;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
