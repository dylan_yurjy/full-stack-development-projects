package com.alphaco.react;

public class Main {
    public interface A {
        default void test() {
            System.out.println("testA");
        }
    }

    public interface B {
        default void test() {
            System.out.println("testB");
        }
    }

    public class Tester implements A, B {
    }
    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
