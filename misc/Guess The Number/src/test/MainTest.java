import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @BeforeAll
    public static void beforeClass() {
        System.out.println("Preparing unit test");
    }

    @BeforeEach
    void setUp() {
        System.out.println("Setting up response() method test");
    }

    @Test
    void groupedAssertions() {
        Main.name = "Blueberry";
        Main.guesses = 5;
        assertAll("Response Method Test",
                () -> assertEquals("Hello! What is your name?\n", Main.responseTest(Res.WELCOME), "Welcome Message Failed"),
                () -> assertEquals("\nOkay, Blueberry, I am thinking of a number between 1 and 20.\nTake a guess.\n", Main.responseTest(Res.START), "Start Message Failed"),
                () -> assertEquals("\nGood job, Blueberry! You guessed my number in 5 guesses!\n\nWould you like to play again? (y/n)\n", Main.responseTest(Res.VICTORY), "Victory Message Failed"),
                () -> assertEquals("\nYour guess is too high.\n", Main.responseTest(Res.LOW), "Too High Failed"),
                () -> assertEquals("\nYour guess is too low.\n", Main.responseTest(Res.HIGH), "Too Low Failed"));

    }

    @AfterEach
    void tearDown() {
        System.out.println("Response method test completed");
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("Systems test complete. Good job!");
    }


}