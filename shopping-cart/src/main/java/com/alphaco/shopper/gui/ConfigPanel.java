package com.alphaco.shopper.gui;

import com.alphaco.shopper.entitys.Product;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.alphaco.shopper.gui.Swing.*;
import static com.alphaco.shopper.gui.Utils.*;

public class ConfigPanel {

    private final JFrame frame;
    private final ProductsPanel productsPanel;
    private final CartPanel cartPanel;
    private final int configCompWidth;
    private final int configCompHeight;
    private final int configComboBoxY;
    private final int configComboLabelY;
    private final int configButtonY;
    private final int numColsConfigPanel;
    private final int numSpacesConfigPanel;
    private final JComboBox<String> categoryFilterBox;
    private final JComboBox<String> priceFilterBox;
    private final JButton purchaseButton;
    private final JButton clearCartButton;
    private final JLabel categoryBoxLabel;
    private final JLabel priceBoxLabel;
    private int configPanelSpacing;

    {
        purchaseButton = new JButton("Checkout");
        clearCartButton = new JButton("Clear Cart");
        categoryFilterBox = new JComboBox<>();
        priceFilterBox = new JComboBox<>();
        categoryBoxLabel = new JLabel("Filter by Category");
        priceBoxLabel = new JLabel("Filter by Price");
        configCompWidth = 100;
        configCompHeight = 30;
        configComboBoxY = (13 * 2) + (configCompHeight) - 15;
        configComboLabelY = (13) + 5;
        configButtonY = 35;
        numColsConfigPanel = 4;
        numSpacesConfigPanel = 5;
    }


    public ConfigPanel(JFrame frame, ProductsPanel productsPanel, CartPanel cartPanel) {
        this.frame = frame;
        this.productsPanel = productsPanel;
        this.cartPanel = cartPanel;
        this.cartPanel.setConfigPanel(this);
        initializeComps();
        initializeListeners();
    }

    private void initializeComps() {
        configPanel.setSize(
                frame.getWidth() - cartScrollPanelWidth - scrollBarWidth + 3,
                configPanelHeight);

        configPanelSpacing = (configPanel.getWidth() - (numColsConfigPanel * 100)) / numSpacesConfigPanel;

        configPanel.setLocation(0, 0);
        configPanel.setLayout(null);

        int compOneX = (configPanelSpacing);
        int compTwoX = (configPanelSpacing * 2) + (configCompWidth);
        int compThreeX = (configPanelSpacing * 3) + (configCompWidth * 2);
        int compFourX = (configPanelSpacing * 4) + (configCompWidth * 3);

        categoryBoxLabel.setLocation(compOneX, configComboLabelY);
        priceBoxLabel.setLocation(compTwoX, configComboLabelY);
        categoryFilterBox.setLocation(compOneX, configComboBoxY);
        priceFilterBox.setLocation(compTwoX, configComboBoxY);
        clearCartButton.setLocation(compThreeX, configButtonY);
        purchaseButton.setLocation(compFourX, configButtonY);

        categoryBoxLabel.setSize(configCompWidth, configCompHeight - 10);
        priceBoxLabel.setSize(configCompWidth, configCompHeight - 10);
        categoryFilterBox.setSize(configCompWidth, configCompHeight);
        priceFilterBox.setSize(configCompWidth, configCompHeight);
        clearCartButton.setSize(configCompWidth, configCompHeight);
        purchaseButton.setSize(configCompWidth, configCompHeight);

        categoryBoxLabel.setHorizontalAlignment(SwingConstants.CENTER);
        priceBoxLabel.setHorizontalAlignment(SwingConstants.CENTER);

        configPanel.add(categoryBoxLabel);
        configPanel.add(priceBoxLabel);
        configPanel.add(categoryFilterBox);
        configPanel.add(priceFilterBox);
        configPanel.add(clearCartButton);
        configPanel.add(purchaseButton);

        ((BasicComboBoxRenderer) categoryFilterBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
        ((BasicComboBoxRenderer) priceFilterBox.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

        categoryFilterBox.setFocusable(false);
        priceFilterBox.setFocusable(false);
        clearCartButton.setFocusable(false);
        purchaseButton.setFocusable(false);

        c.add(configPanel);
    }

    public void updateFilters() {
        String[] cats = productsPanel.getProductList().stream()
                .map(e -> ((Product) e).getCategory())
                .distinct()
                .map(  e -> e.equals("smartphones") ? "no filter,smartphones" : e)
                .collect(StringBuilder::new,
                        (a, b) -> a.append(b).append(","),
                        (a, b) -> {})
                .toString()
                .split(",");

        for (var item : cats) {
            categoryFilterBox.addItem(item);
        }


        String[] prices = { "no cap", "<$100.00", "<$500.00", "<$1,000.00"};
        for (var price : prices) {
            priceFilterBox.addItem(price);
        }
    }

    private void repaintConfigButtons() {
        configPanelSpacing = (configPanel.getWidth() - (numColsConfigPanel * 100)) / numSpacesConfigPanel;

        int compOneX = (configPanelSpacing);
        int compTwoX = (configPanelSpacing * 2) + (configCompWidth);
        int compThreeX = (configPanelSpacing * 3) + (configCompWidth * 2);
        int compFourX = (configPanelSpacing * 4) + (configCompWidth * 3);

        categoryBoxLabel.setLocation(compOneX, configComboLabelY);
        priceBoxLabel.setLocation(compTwoX, configComboLabelY);
        categoryFilterBox.setLocation(compOneX, configComboBoxY);
        priceFilterBox.setLocation(compTwoX, configComboBoxY);
        clearCartButton.setLocation(compThreeX, configButtonY);
        purchaseButton.setLocation(compFourX, configButtonY);
    }

    private void repaintConfigPanelSize() {
        configPanel.setSize(
                frame.getWidth() - cartScrollPanel.getWidth() - scrollBarWidth + 3,
                configPanelHeight);
    }

    public void repaintPanel() {
        repaintConfigPanelSize();
        repaintConfigButtons();
    }

    private void initializeListeners() {
        ActionListener categoryListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filter = ((JComboBox<?>) e.getSource()).getSelectedItem().toString();
                if (!filter.equals("no filter")) {
                    productsPanel.refreshList(filter);
                } else {
                    productsPanel.refreshList("no filter");
                }
            }
        };

        ActionListener priceListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filter = ((JComboBox<?>) e.getSource()).getSelectedItem().toString();
                if (filter.equals("no cap")) {
                    productsPanel.refreshList(0);
                } else {
                    String subString = filter.substring(1);
                    productsPanel.refreshList(currencyParserInt(subString));
                }
            }
        };

        ActionListener clearListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (clearCartRequest()) {
                    cartPanel.clearCart();
                } else {
                    for (var item : Thread.currentThread().getStackTrace()) {
                        System.out.println(item);
                    }
                }
            }
        };

        ActionListener purchaseListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cartPanel.getTotal() > 0) {
                    if (JOptionPane.showConfirmDialog(
                            frame,
                            String.format(
                                    "Your total is %s, would you like to continue?",
                                    formatter.format(cartPanel.getTotal()))) == 0) {
                        JOptionPane.showMessageDialog(
                                frame,
                                "Thank you for shopping with us! Have a great day!");
                        clearCartButton.doClick();
                    }
                } else {
                    JOptionPane.showMessageDialog(
                            frame,
                            "You haven't added anything to your cart!");
                }
            }
        };

        priceFilterBox.addActionListener(priceListener);
        clearCartButton.addActionListener(clearListener);
        categoryFilterBox.addActionListener(categoryListener);
        purchaseButton.addActionListener(purchaseListener);

    }

    public CartPanel getCartPanel() {
        return cartPanel;
    }

    public ProductsPanel getProductsPanel() {
        return productsPanel;
    }
}
