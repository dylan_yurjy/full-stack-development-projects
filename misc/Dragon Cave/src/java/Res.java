public enum Res {
    START, VICTORY, DEFEAT, ERROR;

    public String getRes() {

        switch (this) {
            case START:
                return "You are in a land full of dragons. In front of you,\n\nyou see two caves. In one cave, " +
                        "the dragon is friendly\n\nand will share his treasure with you. The other dragon\n\nis greedy and " +
                        "hungry and will eat you on sight.\n\nWhich cave will you go into? (1 or 2)\n";
            case DEFEAT:
                return "\nYou approach a cave...\n\nIt is dark and spooky...\n\nA large dragon jumps out" +
                        " in front of you! He opens his jaws and...\n\nGobbles you down in one bite!\n";
            case VICTORY:
                return "\nYou chose the right cave!\n\nYou win!\n";
            case ERROR:
                return "Error: Wrong choice. Try again.\n";
        }

        return null;
    }
}
