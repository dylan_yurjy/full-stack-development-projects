package hangman;

import static hangman.Functions.*;
import static hangman.Variables.*;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        name = retrieveName();
        importData();
        play();
    }

    /*
    This function is called at the beginning of each round of Hangman. It displays the starting title, generates both the
    secret word and the respective player's word, then initiates the game loop.
     */
    public static void play() throws IOException {
        System.out.println(retrieveStartTitle());
        computerWord = chooseSecretWord(generateRandomIndex()).toLowerCase();
        playerWord = generatePlayerWord();
        gameLoop();
    }

    /*
    Main loop that runs as long as the variable hasWon is null. First, information about the current state is displayed to the console.
    Then it enters a functional do-while loop requesting input from the user. After correct input is received, the input is checked
    against the secret word and list of current guesses. If the player chooses a correct or incorrect letter, the stage will be altered and then analyzed to see if
    the current stage is a winning or losing state.
     */
    public static void gameLoop() throws IOException {
        while (hasWon == null) {
            displayInformation();
            receiveGuess();
            if (!checkRepeated()) {
                checkContains();
                analyzeProgress();
            }
        }
        displayInformation();
        gameOver();
    }
}
