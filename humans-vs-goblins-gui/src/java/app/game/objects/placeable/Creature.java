package game.objects.placeable;

import game.Environment;
import game.ai.GoblinAI;
import game.utils.enums.Stat;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Image;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

public class Creature extends GameObject {
    private final HashMap<Stat, Integer> statsMap = new HashMap<>();
    private boolean alive = true;

    {
        statsMap.put(Stat.CURRENT_HEALTH, 100);
        statsMap.put(Stat.MAX_HEALTH, 100);
        statsMap.put(Stat.STRENGTH, 10);
    }

    public Creature(Environment env, int x, int y, Image rep) {
        super(env, x, y, rep);
    }

    public Creature(Environment env, Image rep) {
        super(env, rep);
    }

    public void lowerHealth(int amount) throws SWTException {

    }

    public void attack(Creature beingAttacked) throws SWTException {
        int damage = (int) ((getStatsMap().get(Stat.STRENGTH) + 1) * ThreadLocalRandom.current().nextFloat());
        if (damage > 0) {
            beingAttacked.lowerHealth(damage);
        }
    }

    public void died() {
        Point deathPoint = new Point(getLocation());
        Environment env = getEnv();
        ArrayList<GameObject> objs = env.getGameObjects();
        addPreviousLocation();
        Environment.goblin = null;
        objs.remove(this);
        getLabel().dispose();
        Goblin.getHealthBar().removePaintListener(((Goblin) this).getPaintListener());
        Environment.human.setSkulls(Environment.human.getSkulls() + 1);
        Environment.goblin = new Goblin(env);
        Loot newLoot = new Loot(getEnv(), deathPoint);
        objs.add(newLoot);
        objs.add(Environment.goblin);
        if (env.getTreasure() == null) {
            Treasure treasure = new Treasure(env);
            env.setTreasure(treasure);
            objs.add(treasure);
        }
        Environment.goblin.repositionHealthBar();
        env.getLand().updateLand();
        env.updateSkullsLabel();
        GoblinAI.initiateAI();
    }

    public HashMap<Stat, Integer> getStatsMap() {
        return statsMap;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
}
