package game.exceptions;

public class InvalidLandManipulation extends Throwable {

    public InvalidLandManipulation(String str, int x, int y) {
        super(String.format("%s x: %s, y: %s", str, x, y));
    }

    public InvalidLandManipulation(String str, char a, int b) {
        super(String.format("%s %s: %s", str, a, b));
    }
}
