package com.alphaco.shopper.service;

import com.alphaco.shopper.dao.CartDao;
import com.alphaco.shopper.dao.CartItemDao;
import com.alphaco.shopper.dao.ProductDao;
import com.alphaco.shopper.dao.UserDao;
import com.alphaco.shopper.entitys.Cart;
import com.alphaco.shopper.entitys.CartItem;
import com.alphaco.shopper.entitys.Product;
import com.alphaco.shopper.entitys.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private CartDao cartDao;
    @Autowired
    private CartItemDao cartItemDao;

    private final NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public List<Product> getProducts() {
        return productDao.findAll();
    }

    public void addUser(String name) {
        User user = userDao.findByName(name);
        if (user == null) {
            user = new User();
            user.setName(name);
            user.setCart(new Cart());
            userDao.save(user);
        } else {
            System.out.println("User already exists");
        }
    }

    @Override
    public void removeUser(String name) {
        User user = userDao.findByName(name);
        System.out.println(user);
        if (user != null) {
            userDao.delete(user);
        } else {
            System.out.println("User not found");
        }
    }

    @Override
    public List<CartItem> getCartContents(String name) {
        Cart cart = userDao.findByName(name).getCart();
        if (cart != null) {
            return new ArrayList<>(cart.getProducts());
        } else {
            return null;
        }
    }

    @Override
    public List<CartItem> addProductToCart(String userName, String prodId) {
        Product product = productDao.getById(Integer.parseInt(prodId));
        if (product.getStock() > 0) {
            CartItem cartItem = new CartItem(1, product);
            Cart cart = userDao.findByName(userName).getCart();
            if (!cart.getProducts().contains(cartItem)) {
                cart.getProducts().add(cartItem);
                float amount = currencyParser(cartItem.getProduct().getPrice());
                cart.addToPrice(amount);
                decrementProductStock(product);
                cartItemDao.save(cartItem);
                cartDao.save(cart);
                return cart.getProducts();
            } else {
                System.out.println("this item is already in cart");
            }
        } else {
            System.out.println("this item is out of stock");
        }
        return null;
    }

    @Override
    public List<CartItem> removeProductFromCart(String userName, String prodId) {
        Product product = productDao.getById(Integer.parseInt(prodId));
        Cart cart = userDao.findByName(userName).getCart();
        List<CartItem> productsInCart = cart.getProducts();
        CartItem cartItem = productsInCart.stream()
                .filter(e -> e.equals(product))
                .findFirst()
                .orElse(null);
        if (cartItem != null) {
            productsInCart.remove(cartItem);
            int amount = cartItem.getQuantity();
            float price = currencyParser(cartItem.getProduct().getPrice()) * amount;
            cart.subtractFromPrice(price);
            incrementProductStock(product, amount);
            cartItemDao.delete(cartItem);
            cartDao.save(cart);
            return productsInCart;
        } else {
            System.out.println("item is not in the cart");
            return null;
        }
    }

    @Override
    public boolean incrementProductQuantity(String userName, String prodId) {
        Product product = productDao.getById(Integer.parseInt(prodId));
        Cart cart = userDao.findByName(userName).getCart();
        List<CartItem> productsInCart = cart.getProducts();
        CartItem cartItem = productsInCart.stream()
                .filter(e -> e.equals(product))
                .findFirst()
                .orElse(null);
        if (cartItem != null) {
            if (product.getStock() > 0) {
                cartItem.setQuantity(cartItem.getQuantity() + 1);
                float amount = currencyParser(cartItem.getProduct().getPrice());
                cart.addToPrice(amount);
                decrementProductStock(product);
                cartItemDao.save(cartItem);
                cartDao.save(cart);
                return true;
            } else {
                System.out.println("item is out of stock");
                return false;
            }
        } else {
            System.out.println("item is not in the cart");
            return false;
        }
    }

    @Override
    public void decrementProductQuantity(String userName, String prodId) {
        Product product = productDao.getById(Integer.parseInt(prodId));
        Cart cart = userDao.findByName(userName).getCart();
        List<CartItem> productsInCart = cart.getProducts();
        CartItem cartItem = productsInCart.stream()
                .filter(e -> e.equals(product))
                .findFirst()
                .orElse(null);
        if (cartItem != null) {
            if (cartItem.getQuantity() == 1) {
                System.out.println("Quantity is one");
                productsInCart.remove(cartItem);
                cartItemDao.delete(cartItem);
                cartDao.save(cart);
            } else {
                cartItem.setQuantity(cartItem.getQuantity() - 1);
                cartItemDao.save(cartItem);
            }
            float amount = currencyParser(cartItem.getProduct().getPrice());
            cart.subtractFromPrice(amount);
            incrementProductStock(product);
        } else {
            System.out.println("item is not in the cart");
        }
    }

    @Override
    public boolean clearCart(String userName) {
        Cart cart = userDao.findByName(userName).getCart();
        List<CartItem> productsInCart = cart.getProducts();
        for (var item : productsInCart) {
            int amountInCart = item.getQuantity();
            Product product = productDao.findById(item.getProduct().getId()).get();
            product.setStock(product.getStock() + amountInCart);
            cartItemDao.delete(item);
            productDao.save(product);
        }
        cart.setTotalPrice(0);
        productsInCart.clear();
        return cartDao.save(cart).getProducts().size() == 0;
    }

    private void incrementProductStock(Product product) {
        product.setStock(product.getStock() + 1);
        productDao.save(product);
    }

    private void decrementProductStock(Product product) {
        product.setStock(product.getStock() - 1);
        productDao.save(product);
    }

    private void incrementProductStock(Product product, int amount) {
        product.setStock(product.getStock() + amount);
        productDao.save(product);
    }

    private void decrementProductStock(Product product, int amount) {
        product.setStock(product.getStock() - amount);
            productDao.save(product);
    }

    private float currencyParser(String amount) {
        try {
            return numberFormat.parse(amount).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0f;
    }
}
