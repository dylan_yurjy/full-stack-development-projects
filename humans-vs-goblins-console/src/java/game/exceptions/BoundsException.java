package game.exceptions;

public class BoundsException extends Exception {

    public BoundsException(String str, int x, int y) {
        super(String.format("%s x: %s, y: %s", str, x, y));
    }

    public BoundsException(String str, char a, int b) {
        super(String.format("%s %s: %s", str, a, b));
    }
}
