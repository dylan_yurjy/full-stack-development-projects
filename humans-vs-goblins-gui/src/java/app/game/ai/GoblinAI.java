package game.ai;

import game.Environment;
import game.utils.Combat;
import game.utils.enums.Direction;
import org.eclipse.swt.SWTException;

import static game.Environment.*;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

@SuppressWarnings("BusyWait")
public class GoblinAI {
    private static Environment env;
    private static final ArrayList<Direction> possibleMoves = new ArrayList<>();

    public static void initializeVariables(Environment env) {
        GoblinAI.env = env;
    }

    public static Thread getAI_Thread() {
        return new Thread(() -> {
            try {
                while (goblin.isAlive() && human.isAlive()) {
                    while (checkForCollision() && goblin.isAlive()) {
                        goblin.attack(human);
                        if (!Combat.isHumanInCombat()) {
                            Combat.setHumanInCombat(true);
                            Combat.initiateCombat();
                        }
                        if (!goblin.isAlive() || !human.isAlive()) {
                            break;
                        }
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (goblin.isAlive()) {
                        env.getDisplay().syncExec(() -> {
                            executeNextMove();
                            env.getLand().updateLand();
                            goblin.repositionHealthBar();
                        });
                    }
                    if (!goblin.isAlive() || !human.isAlive()) {
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (human.isAlive()) {
                    env.getDisplay().syncExec(() -> goblin.died());
                } else {
                    goblin.setAlive(false);
                    env.gameOver();
                }
            } catch (SWTException e) {
                System.out.println("Exception in Goblin AI Thread");
            }
        });
    }

    public static void initiateAI() {
        getAI_Thread().start();
    }

    public static boolean checkForCollision() {
        if (goblin == null) {
            return false;
        }
        int humanX = human.getLocationX();
        int humanY = human.getLocationY();
        int goblinX = goblin.getLocationX();
        int goblinY = goblin.getLocationY();

        if (Math.abs(humanX - goblinX) <= 1 && humanY - goblinY == 0) {
            return true;
        } else if (Math.abs(humanY - goblinY) <= 1 && humanX - goblinX == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void executeNextMove() {
        int humanX = human.getLocationX();
        int humanY = human.getLocationY();
        int goblinX = goblin.getLocationX();
        int goblinY = goblin.getLocationY();

        if (humanX > goblinX) {
            possibleMoves.add(Direction.RIGHT);
        } else if (humanX < goblinX) {
            possibleMoves.add(Direction.LEFT);
        }
        if (humanY < goblinY) {
            possibleMoves.add(Direction.UP);
        } else if (humanY > goblinY) {
            possibleMoves.add(Direction.DOWN);
        }

        if (possibleMoves.size() > 0) {
            goblin.move(possibleMoves.get(ThreadLocalRandom.current().nextInt(possibleMoves.size())));
        }

        possibleMoves.clear();
    }
}
