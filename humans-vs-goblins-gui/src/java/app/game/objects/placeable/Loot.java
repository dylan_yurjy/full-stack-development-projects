package game.objects.placeable;

import game.Environment;
import game.utils.enums.Items;

import java.awt.*;
import java.util.ArrayList;

public class Loot extends Drop {
    private final ArrayList<Items> items = new ArrayList<>();

    public Loot(Environment env, Point point) {
        super(env, point, env.getImageLoader().getDropIcon());
        int score = Environment.human.getSkulls();
        generateLoot(score);
    }

    public void generateLoot(int score) {
        switch (score) {
            case 1:
                items.add(Items.SILVER_SWORD);
                items.add(Items.SMALL_HEALTH_POTION);
                break;
            case 2:
                items.add(Items.GOLD_SWORD);
                items.add(Items.MEDIUM_HEALTH_POTION);
                break;
            default:
                items.add(Items.LARGE_HEALTH_POTION);
        }
    }

    public ArrayList<Items> getItems() {
        return items;
    }
}
