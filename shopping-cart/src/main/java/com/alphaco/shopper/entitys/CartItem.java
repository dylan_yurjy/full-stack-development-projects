package com.alphaco.shopper.entitys;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class CartItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int quantity;
    @OneToOne
    private Product product;

    public CartItem() {}

    public CartItem(int quantity, Product product) {
        this.quantity = quantity;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o instanceof Product) return product.getId() == ((Product) o).getId();
        if (this.getClass() != o.getClass()) return false;
        CartItem cartItem = (CartItem) o;
        return product.getId() == cartItem.getProduct().getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(product);
    }
}
