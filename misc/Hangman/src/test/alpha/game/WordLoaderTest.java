package alpha.game;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class WordLoaderTest {

    @RepeatedTest(value = 20, name = "#{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("Repeated Word Loader")
    void secretWord() {
        try {
            assertTrue(WordLoader.secretWord().substring(0, 6) != "Error", "Error Retrieving Word From JSON File.");
        } catch (IndexOutOfBoundsException e) {
            assertTrue(true);
        }
    }
}