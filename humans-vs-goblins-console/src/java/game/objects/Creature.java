package game.objects;

import java.util.HashMap;

public class Creature extends Placeable {

    HashMap<String, Integer> stats = new HashMap<>();

    {
        stats.put("currentHealth", 100);
        stats.put("maxHealth", 100);
        stats.put("strength", 10);
    }

    public Creature(char rep, int x, int y) {
        super(rep, x, y);
    }

    public Creature(char rep, int x, int y, int str) {
        super(rep, x, y);
        stats.replace("strength", str);
    }

    public void attack(Creature creature) {
        HashMap<String, Integer> enemyStats = creature.getStats();
        int thisAttackDamage;
        thisAttackDamage = (int) (Math.random() * stats.get("strength"));
        enemyStats.replace("currentHealth", Math.max(enemyStats.get("currentHealth") - thisAttackDamage, 0));
    }

    public HashMap<String, Integer> getStats() {
        return stats;
    }

}
