import java.io.IOError;
import java.io.IOException;
import java.util.Scanner;

public class DragonCave {
    String input = "0";
    boolean acceptable = false;
    Scanner scanner = new Scanner(System.in);

    public DragonCave() {
        play();
    }

    private void play() {
        System.out.println(response(Res.START));
        while (!acceptable) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (input.equals("1") || input.equals("2")) {
                choice(input);
            } else {
                System.out.println(response(Res.ERROR));
            }
        }
    }

    private void victory() {
        System.out.println(response(Res.VICTORY));
    }

    private void defeat() {
        System.out.println(response(Res.DEFEAT));
    }

    private void choice(String c) {
        if (c.equals("1")) {
            defeat();
        } else {
            victory();
        }
        System.exit(0);
    }

    private String response(Res res) {
        return res.getRes();
    }

    public static String responseTest(Res res) {
        return res.getRes();
    }

    public static void main(String[] args) {
        new DragonCave();
    }
}
