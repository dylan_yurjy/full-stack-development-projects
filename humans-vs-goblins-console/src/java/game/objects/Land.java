package game.objects;

import game.exceptions.BoundsException;
import game.exceptions.InvalidLandManipulation;
import game.exceptions.OccupiedSpaceException;
import game.utils.Location;
import game.utils.MessagePrinter;

import java.util.Arrays;

public class Land {

    private final char[][] land;
    private MessagePrinter printer = new MessagePrinter();
    private Location humanLoc;
    private Location goblinLoc;

    {
        land = new char[15][30];
        int rowLength = 30;
        for (int i = 0; i < land.length; i++) {
            if (i == 0 || i == land.length - 1) {
                Arrays.fill(land[i], '|');
            } else {
                Arrays.fill(land[i], ' ');
            }
            land[i][0] = '|';
            land[i][1] = '|';
            land[i][rowLength - 2] = '|';
            land[i][rowLength - 1] = '|';
        }
    }

    public Land() {}

    @SuppressWarnings("unused")
    public Land(boolean devMode) {}

    public void renderLand() {
        int i = 0;
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        for (var row : land) {
            for (var space : row) {
                System.out.print(space);
            }
            printer.printRowMessage(i);
            System.out.println();
            i++;
        }
    }

    protected boolean editLand(char c, int x, int y) {
        try {
            validateManipulation(x, y);
        } catch (InvalidLandManipulation e) {
            System.out.println(e.getMessage());
            return false;
        }
        getLand()[x][y] = c;
        return true;
    }

    public MessagePrinter getPrinter() {
        return printer;
    }

    public char[][] getLand() {
        return land;
    }

    protected char getLand(int x, int y) {
        return land[x][y];
    }

    public void setHumanLoc(Location humanLoc) {
        this.humanLoc = humanLoc;
    }

    public void setGoblinLoc(Location goblinLoc) {
        this.goblinLoc = goblinLoc;
    }

    public void setPrinter(MessagePrinter printer) {
        this.printer = printer;
    }


    public boolean moveValidator(String input) {
        switch (input) {
            case "w" -> {
                if (humanLoc.getX() == 1) {
                    return false;
                }
            }
            case "d" -> {
                if (humanLoc.getY() == 27) {
                    return false;
                }
            }
            case "s" -> {
                if (humanLoc.getX() == 13) {
                    return false;
                }
            }
            case "a" -> {
                if (humanLoc.getY() == 2) {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean validateManipulation(int x, int y) throws InvalidLandManipulation {
        if ((x < 1 || x > 13) && (y < 2 || y > 27)) {
            throw new InvalidLandManipulation("You cannot alter this part of the land.", x, y);
        } else if (x < 1 || x > 13) {
            throw new InvalidLandManipulation("You cannot alter this part of the land.", 'x', x);
        } else if (y < 2 || y > 27) {
            throw new InvalidLandManipulation("You cannot alter this part of the land.", 'y', y);
        }
        return true;
    }

    protected boolean validateLocation(int x, int y) throws OccupiedSpaceException, BoundsException {
        if (getLand(x, y) != ' ') {
            throw new OccupiedSpaceException("Object could not be generated. Location is occupied", x, y);
        }
        if ((x < 1 || x > 13) && (y < 2 || y > 27)) {
            throw new BoundsException("Object could not be generated. Location out of bounds.", x, y);
        } else if (x < 1 || x > 13) {
            throw new BoundsException("Object could not be generated. Location out of bounds.", 'x', x);
        } else if (y < 2 || y > 27) {
            throw new BoundsException("Object could not be generated. Location out of bounds.", 'y', y);
        }
        return true;
    }
}
