package game.objects.placeable;

import game.Environment;
import game.utils.enums.HudComponent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import java.awt.*;

public class Drop extends GameObject {

    public static Label label;

    public Drop(Environment env, Point point, Image rep) {
        super(env, point.x, point.y, rep);
    }

    public Drop(Environment env, Image rep) {
        super(env, rep);
    }

    public void transferContents(Environment env) {
        Human human = Environment.human;
        switch (this.getClass().getSimpleName()) {
            case "Treasure":
                Treasure treasure = (Treasure) this;
                human.setGold(human.getGold() + treasure.getGold());
                treasure.getLabel().dispose();
                env.setTreasure(null);
                env.getOldLocations().add(treasure.getLocation());
                env.updateCoinsLabel();
                break;
            case "Loot":
                Loot drop = (Loot) this;
                if (human.getItems().size() == 7) {
                    // do nothing
                } else {
                    if (human.pickupItems(drop.getItems())) {
                        drop.getLabel().dispose();
                        env.getOldLocations().add(drop.getLocation());
                    } else {
                        // do nothing
                    }
                }
                break;
        }
    }
}
