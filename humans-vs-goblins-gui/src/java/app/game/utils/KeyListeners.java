package game.utils;

import game.Environment;
import game.objects.placeable.Human;
import game.utils.enums.Direction;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class KeyListeners {

    private final static Listener keyListener;
    private final static Environment env = Environment.env;

    static {
        keyListener = new Listener() {
            @Override
            public void handleEvent(Event event) {
                char value = Character.toLowerCase(event.character);
                Human human = Environment.human;
                boolean moved = false;
                switch (value) {
                    case 'w':
                        human.move(Direction.UP);
                        moved = true;
                        break;
                    case 'd':
                        human.move(Direction.RIGHT);
                        moved = true;
                        break;
                    case 'a':
                        human.move(Direction.LEFT);
                        moved = true;
                        break;
                    case 's':
                        human.move(Direction.DOWN);
                        moved = true;
                        break;
                }
                if (moved) {
                    if (!Combat.isHumanInCombat()) {
                        env.getLand().checkForCombatInitiation();
                    }
                    env.getLand().checkForDropCollection();
                    env.getLand().updateLand();
                }
            }
        };
    }

    public static void addKeyListeners() {
        env.getDisplay().addFilter(SWT.KeyDown, KeyListeners.keyListener);
    }
    public static void removeKeyListeners() {
        env.getDisplay().removeFilter(SWT.KeyDown, KeyListeners.keyListener);
    }
}
