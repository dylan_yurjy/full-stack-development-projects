package game.objects.placeable;

import game.Environment;
import game.objects.items.BronzeSword;
import game.objects.items.Item;
import game.utils.enums.HudComponent;
import game.utils.enums.InventorySlot;
import game.utils.enums.Items;
import game.utils.enums.Stat;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.Iterator;

public class Human extends Creature {
    private int gold = 0;
    private int skulls = 0;
    private final ArrayList<Item> items = new ArrayList<>();
    private Item equippedItem = null;



    public Human(Environment env, int x, int y) {
        super(env, x, y, env.getImageLoader().getHumanIcon());
        Label label = (Label) env.getWidgetMap().get(HudComponent.COINS_CANVAS).getChildren()[1];
        label.setText(String.valueOf(getGold()));
    }

    public boolean pickupItems(ArrayList<Items> loot) {
        boolean diminished = true;
        Iterator<Items> iter = loot.iterator();
        while (iter.hasNext()) {
            if (getItems().size() < 7) {
                items.add(iter.next().generateItem(getEnv()));
                iter.remove();
            } else {
                diminished = false;
            }
        }
        return diminished;
    }

    public void reorderSlots() {
        getItems().forEach( e -> {
            e.setSlot(InventorySlot.values()[getItems().indexOf(e)]);
            e.repositionLabel();
        });
    }

    public void acquireDropContents(Environment env, Drop obj) {
        obj.transferContents(env);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    @Override
    public void lowerHealth(int amount) throws SWTException {
        int currentHealth = getStatsMap().get(Stat.CURRENT_HEALTH);
        int newHealth = Math.max(currentHealth - amount, 0);
        if (newHealth == 0) {
            died();
        } else {
            getStatsMap().put(Stat.CURRENT_HEALTH, newHealth);
            Environment env = getEnv();
            env.getDisplay().syncExec(() -> env.getWidgetMap().get(HudComponent.HUMAN_HEALTH_BAR).redraw());
        }
    }

    @Override
    public void died() {
        setAlive(false);
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getSkulls() {
        return skulls;
    }

    public void setSkulls(int skulls) {
        this.skulls = skulls;
    }

    public Item getEquippedItem() {
        return equippedItem;
    }

    public void setEquippedItem(Item equippedItem) {
        this.equippedItem = equippedItem;
    }
}
