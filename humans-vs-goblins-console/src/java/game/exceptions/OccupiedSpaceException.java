package game.exceptions;

public class OccupiedSpaceException extends Throwable {
    public OccupiedSpaceException(String str, int x, int y) {
        super(String.format("%s x: %s, y: %s", str, x, y));
    }
}
