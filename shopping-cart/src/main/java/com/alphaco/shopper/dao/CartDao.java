package com.alphaco.shopper.dao;

import com.alphaco.shopper.entitys.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartDao extends JpaRepository<Cart, Integer> {
}
