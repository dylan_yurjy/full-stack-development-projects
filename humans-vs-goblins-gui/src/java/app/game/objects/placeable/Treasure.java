package game.objects.placeable;

import game.Environment;

import java.util.concurrent.ThreadLocalRandom;

public class Treasure extends Drop {
    private final int gold;

    {
        this.gold = ThreadLocalRandom.current().nextInt(1,11) * 10;
    }

    public Treasure(Environment env) {
        super(env, env.getImageLoader().getTreasureIcon());
    }

    public int getGold() {
        return gold;
    }
}
