package com.alphaco.shopper.gui;

import com.alphaco.shopper.entitys.CartItem;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static com.alphaco.shopper.gui.Swing.*;
import static com.alphaco.shopper.gui.CartCard.numCartCards;
import static com.alphaco.shopper.gui.Utils.*;


public class CartPanel {
    private final JFrame frame;
    private final JLabel totalLabel;
    private ConfigPanel configPanel;
    private List<?> cartItems;
    private int total;

    public CartPanel(JFrame frame) {
        this.frame = frame;
        this.total = 0;
        this.totalLabel = new JLabel(String.format("Current Total: %s", formatter.format(total)));
        initialize();
    }


    public void initialize() {
        cartScrollPanel.setSize(
                cartScrollPanelWidth,
                frame.getHeight() - scrollBarWidth - 22 + 3);

        cartScrollPanel.setLocation(
                frame.getWidth() - cartScrollPanel.getWidth() - scrollBarWidth + 3,
                -1);

        cartScrollPanel.setVerticalScrollBar(new JScrollBar(Adjustable.VERTICAL));
        cartScrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        cartScrollPanel.getVerticalScrollBar().setUnitIncrement(40);

        totalLabel.setFont(brandFont);
        totalLabel.setSize(totalLabel.getPreferredSize());
        totalLabel.setLocation((cartScrollPanelWidth - totalLabel.getWidth()) / 2 - (scrollBarWidth/2), 7);

        cartClientView.add(totalLabel);

        cartClientView.setLayout(null);
        JViewport cartViewport = cartScrollPanel.getViewport();
        cartViewport.setView(cartClientView);

        c.add(cartScrollPanel);
    }

    public void requestCart() {
        refreshCart(getCartList());
    }

    public void injectCartItems() {
        int numInCart = cartItems.size();
        int cartClientViewHeight = (numInCart * 80) + (numInCart * 20) + 40;
        cartClientView.setPreferredSize(new Dimension(0, cartClientViewHeight));

        for (var item : cartItems) {
            CartItem cartItem = (CartItem) item;
            CartCard cartCard = new CartCard(
                    this,
                    cartItem.getProduct(),
                    cartScrollPanelWidth,
                    scrollBarWidth,
                    cartItem.getQuantity());
            int itemQty = cartItem.getQuantity();
            float itemPrice = currencyParser(cartItem.getProduct().getPrice());
            itemPrice *= itemQty;
            total += itemPrice;
            cartClientView.add(cartCard);
            cartClientView.repaint();
        }
        reconfigureTotalLabel();
        cartClientView.add(totalLabel);
        cartClientView.repaint();
    }

    public void refreshCart(List<?> cartSet) {
        this.cartItems = cartSet;
        numCartCards = 0;
        total = 0;
        destroyCartComps();
        injectCartItems();
    }

    public void clearCart() {
        if (cartItems != null) {
            for (var item : cartClientView.getComponents()) {
                if (item instanceof CartCard) {
                    CartCard cartCard = (CartCard) item;
                    configPanel.getProductsPanel().updateStock(cartCard.getQty(), cartCard.getProduct());
                }
            }
            cartItems.clear();
            this.total = 0;
            destroyCartComps();
            reconfigureTotalLabel();
            cartClientView.add(totalLabel);
            cartClientView.repaint();
        }
    }

    public void destroyCartComps() {
        cartClientView.removeAll();
        cartClientView.revalidate();
    }

    public void repaintCartPanelSize() {
        cartScrollPanel.setSize(
                cartScrollPanel.getWidth(),
                frame.getHeight() - scrollBarWidth - 22 + 3);
    }

    public void repaintCartPanelLocation() {
        cartScrollPanel.setLocation(
                frame.getWidth() - cartScrollPanel.getWidth() - scrollBarWidth + 3,
                0);
    }

    private void reconfigureTotalLabel() {
        totalLabel.setText(String.format("Current Total: %s", formatter.format(total)));
        totalLabel.setSize(totalLabel.getPreferredSize());
        totalLabel.setLocation((cartScrollPanelWidth - totalLabel.getWidth()) / 2 - (scrollBarWidth/2), 7);
    }


    public void addToTotal(float amount) {
        this.total += amount;
        totalLabel.setText(String.format("Current Total: %s", formatter.format(total)));
    }


    public void subtractFromTotal(float amount) {
        this.total -= amount;
        totalLabel.setText(String.format("Current Total: %s", formatter.format(total)));
    }

    public int getTotal() {
        return total;
    }

    public List<?> getCartItems() {
        return cartItems;
    }

    public void setConfigPanel(ConfigPanel configPanel) {
        this.configPanel = configPanel;
    }

    public ConfigPanel getConfigPanel() {
        return configPanel;
    }

    public JFrame getFrame() {
        return frame;
    }
}
