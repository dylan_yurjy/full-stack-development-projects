package game.objects;

import java.util.HashMap;

    public class Item {

        private final String name;
        private final boolean consumable;
        private final HashMap<String, Integer> boostMap = new HashMap<>();

        public Item(String name, boolean consumable, String... args) {
            this.name = name;
            this.consumable = consumable;
            for (int i = 0; i < args.length; i++) {
                if (i % 2 == 0) {
                    boostMap.put(args[i], Integer.parseInt(args[i+1]));
                }
            }
        }

        public String getName() {
            return name;
        }

        public boolean isConsumable() {
            return consumable;
        }

        public HashMap<String, Integer> getBoostMap() {
            return boostMap;
        }
    }
