package game.objects;

import game.utils.*;

import java.util.ArrayList;
public class Environment {

    private boolean active = true;
    private final Land land;
    private final ArrayList<Placeable> placedObjects = new ArrayList<>();
    private final ArrayList<Location> oldLocations = new ArrayList<>();
    private final InputProcessor inputProcessor = new InputProcessor();
    private Human human;
    private Goblin goblin;

    public Environment() {
        this.land = new Land();
        human = Human.spawnHuman(this, 13, 9);
        goblin = Goblin.spawnGoblin(this, 1, 8);
        Gold.spawnTreasure(this);
        updateLand();
        mainLoop();
    }

    private void restart() {
        human = null;
        goblin = null;
        for (var object : placedObjects) {
            oldLocations.add(object.getLocation());
        }
        placedObjects.clear();
        updateLand();
        land.setPrinter(null);
        land.setPrinter(new MessagePrinter());
        setActive(true);
        human = Human.spawnHuman(this, 13, 9);
        goblin = Goblin.spawnGoblin(this, 1, 8);
        Gold.spawnTreasure(this);
        updateLand();
        mainLoop();
    }

    @SuppressWarnings("unused")
    public Environment(boolean devMode) {
        this.land = new Land(true);
    }

    private void mainLoop() {
        while (active) {
            processInput();
            if (!active) {
                break;
            }
            executeMoves();
            executeCombatFunctionality();
            updateLand();
        }
        gameOver();
    }

    public void gameOver() {
        if (inputProcessor.gameOver()) {
            restart();
        } else {
            System.out.println("Goodbye!");
            System.exit(0);
        }
    }

    public void executeCombatFunctionality() {
        if (Combat.getInCombat()) {
            Combat.battle(this,human, goblin);
        }
    }

    public void executeMoves() {
        Moves.executeMoves(this, inputProcessor.getInput(), human, goblin);
    }

    public void processInput() {
        inputProcessor.receiveInput(land, this);
    }

    public void updateLand() {
        for (var item : oldLocations) {
            int x = item.getX();
            int y = item.getY();
            land.editLand(' ', x, y);
        }
        oldLocations.clear();
        for (var item : placedObjects) {
            int x = item.getLocation().getX();
            int y = item.getLocation().getY();
            land.editLand(item.toString().charAt(0), x, y);
            switch (item.getClass().getSimpleName()) {
                case "Human" -> land.setHumanLoc(item.getLocation());
                case "Goblin" -> land.setGoblinLoc(item.getLocation());
            }
        }
        if (!Combat.getInCombat()) {
            land.renderLand();
        }
    }

    public boolean isActive() {
        return active;
    }

    public ArrayList<Location> getOldLocations() {
        return oldLocations;
    }

    public ArrayList<Placeable> getPlacedObjects() {
        return placedObjects;
    }

    public InputProcessor getInputProcessor() {
        return inputProcessor;
    }

    public Land getLand() {
        return land;
    }

    public Human getHuman() {
        return human;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setHuman(Human human) {
        this.human = human;
    }

    public void setGoblin(Goblin goblin) {
        this.goblin = goblin;
    }
}
