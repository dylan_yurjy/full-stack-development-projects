package com.alphaco.shopper.gui;

import com.alphaco.shopper.entitys.Product;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.alphaco.shopper.gui.Swing.*;
import static com.alphaco.shopper.gui.Utils.currencyParserInt;

public class ProductsPanel {

    private final Swing frame;
    private final JPanel clientView = new JPanel();
    private List<?> productList;
    private final List<ProductCard> productCards;
    private List<ProductCard> filteredProductCards;
    private int numProds = 0;
    private boolean refresh;
    private boolean catFilter;
    private boolean numFilter;
    private String catFilterString;
    private int numFilterInt;


    public ProductsPanel(Swing frame) {
        this.frame = frame;
        this.productCards = new ArrayList<>();
        initialize();
    }

    private void initialize() {

        productsScrollPane.setSize(
                frame.getWidth() - cartScrollPanel.getWidth() - scrollBarWidth + 6,
                frame.getHeight() - configPanelHeight - scrollBarWidth - 22 + 3);

        productsScrollPane.setLocation(
                -1,
                configPanelHeight);

        productsScrollPane.setVerticalScrollBar(new JScrollBar(Adjustable.VERTICAL));
        productsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        productsScrollPane.getVerticalScrollBar().setUnitIncrement(40);

        clientView.setPreferredSize(new Dimension(0, 0));
        clientView.setLayout(null);

        JViewport productViewport = productsScrollPane.getViewport();
        productViewport.setView(clientView);

        c.add(productsScrollPane);
    }

    public void repaintProductPanelSize() {
        productsScrollPane.setSize(
                frame.getWidth() - cartScrollPanel.getWidth() - scrollBarWidth + 6,
                frame.getHeight() - configPanelHeight - scrollBarWidth - 22 + 3);
    }

    public void repaintProductPanels() {
        int numCols = (productsScrollPane.getWidth() - ((((productsScrollPane.getWidth() / 400)) + 1) * 20)) / 400;
        if (refresh || numCols != oldNumCols) {
            int numRows = (int) Math.ceil((float) numProds / (float) numCols);
            int scrollPaneHeight = (numRows * 200) + ((numRows + 1) * 20);
            clientView.setPreferredSize(new Dimension(0, scrollPaneHeight));
            for (var item : clientView.getComponents()) {
                ProductCard productCard = (ProductCard) item;
                int index = productCard.getIndex();
                int colNum = index % numCols == 0 ? numCols : index % numCols;
                int rowNum = (int) Math.ceil((float) index / numCols);
                int spacing = (productsScrollPane.getWidth() - (numCols * 400)) / ((numCols) + 1);
                int locationX = ((colNum - 1) * 400) + (colNum * spacing);
                int locationY = ((rowNum - 1) * 200) + (rowNum * 20);
                item.setLocation(locationX, locationY);
            }
            oldNumCols = numCols;
            refresh = false;
        }
    }

    public void repaintPanel() {
        repaintProductPanelSize();
        repaintProductPanels();
    }

    public void injectProducts(List<?> list, ConfigPanel config) {
        this.productList = list;
        config.updateFilters();
        this.numProds = list.size();
        int numCols = (productsScrollPane.getWidth() - ((((productsScrollPane.getWidth() / 400)) + 1) * 20)) / 400;
        int numRows = (int) Math.ceil((float) numProds / (float) numCols);
        oldNumCols = numCols;
        int scrollPaneHeight = (numRows * 200) + ((numRows + 1) * 20);
        productsScrollPane.getVerticalScrollBar().setUnitIncrement(40);

        clientView.setPreferredSize(new Dimension(0, scrollPaneHeight));

        for (int i = 0; i < list.size(); i++) {
            try {
                ProductCard productCard = new ProductCard(
                        config.getCartPanel(),
                        (Product) list.get(i),
                        numCols,
                        i + 1,
                        productsScrollPane.getWidth());
                        clientView.add(productCard);
                        productCards.add(productCard);
                        clientView.repaint();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void injectProducts(List<ProductCard> list) {
        this.filteredProductCards = list;
        this.numProds = filteredProductCards.size();
        int numCols = (productsScrollPane.getWidth() - ((((productsScrollPane.getWidth() / 400)) + 1) * 20)) / 400;
        oldNumCols = numCols;
        int numRows = (int) Math.ceil((float) numProds / (float) numCols);
        int scrollPaneHeight = (numRows * 200) + ((numRows + 1) * 20);

        clientView.setPreferredSize(new Dimension(0, scrollPaneHeight));

        int i = 0;
        for (ProductCard filteredProductCard : filteredProductCards) {
            filteredProductCard.setIndex(i + 1);
            clientView.add(filteredProductCard);
            clientView.repaint();
            i++;
        }
        repaintPanel();
        productsScrollPane.getViewport().revalidate();
        clientView.repaint();
    }


    public void refreshList(String filter) {
        if (filter.equals("no filter")) {
            System.out.println(filter);
            catFilterString = null;
            catFilter = false;
            if (filteredProductCards != null) {
                if (!(filteredProductCards.size() == productCards.size())) {
                    if (numFilter) {
                        List<ProductCard> newList = productCards.stream()
                                .filter(e -> {
                                    int parsed = currencyParserInt(e.getProduct().getPrice());
                                    return parsed < numFilterInt;
                                })
                                .collect(Collectors.toList());
                        destroyProductComps();
                        refresh = true;
                        injectProducts(newList);
                    } else {
                        destroyProductComps();
                        refresh = true;
                        injectProducts(productCards);
                    }
                }
            }
        } else {
            catFilterString = filter;
            catFilter = true;
            List<ProductCard> newList;
            if (numFilter) {
                newList = productCards.stream()
                        .filter(e -> {
                            int parsed = currencyParserInt(e.getProduct().getPrice());
                            return parsed < numFilterInt && e.getProduct().getCategory().equals(catFilterString);
                        })
                        .collect(Collectors.toList());
            } else {
                newList = productCards.stream()
                        .filter(e -> e.getProduct().getCategory().equals(filter))
                        .collect(Collectors.toList());
            }
            destroyProductComps();
            refresh = true;
            injectProducts(newList);
        }
    }

    public void refreshList(int filter) {
        if (filter != 0) {
            numFilterInt = filter;
            numFilter = true;
            List<ProductCard> newList;
            if (catFilter) {
                newList = filteredProductCards.stream()
                        .filter(e -> {
                            int parsed = currencyParserInt(e.getProduct().getPrice());
                            return parsed < filter && e.getProduct().getCategory().equals(catFilterString);
                        })
                        .collect(Collectors.toList());
            } else {
                newList = productCards.stream()
                        .filter(e -> {
                            int parsed = currencyParserInt(e.getProduct().getPrice());
                            return parsed < filter;
                        })
                        .collect(Collectors.toList());
            }
            destroyProductComps();
            refresh = true;
            injectProducts(newList);
        } else {
            numFilter = false;
            numFilterInt = filter;
            if (filteredProductCards != null) {
                if (filteredProductCards.size() != productCards.size()) {
                    if (catFilter) {
                        List<ProductCard> newList = productCards.stream()
                                .filter( e -> e.getProduct().getCategory().equals(catFilterString))
                                .collect(Collectors.toList());
                        destroyProductComps();
                        refresh = true;
                        injectProducts(newList);
                    } else {
                        destroyProductComps();
                        refresh = true;
                        injectProducts(productCards);
                    }
                }
            }
        }
    }

    public void destroyProductComps() {
        clientView.removeAll();
        clientView.validate();
    }

    public List<?> getProductList() {
        return productList;
    }

    public void updateStock(int value, Product product) {
        for (var item : productCards) {
            if (item.getProduct().getId() == product.getId()) {
                Product product1 = item.getProduct();
                product1.setStock(product1.getStock() + value);
                item.getStockLabel().setText(String.format("Stock: %s", product1.getStock()));
                break;
            }
        }
    }

    public boolean checkStock(int id) {
        for (var item : productCards) {
            if (item.getProduct().getId() == id) {
                return item.getProduct().getStock() > 0;
            }
        }
        return false;
    }
}
