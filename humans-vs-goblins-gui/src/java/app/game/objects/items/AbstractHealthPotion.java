package game.objects.items;

import game.utils.enums.Stat;

public abstract class AbstractHealthPotion extends AbstractPotion {

    public AbstractHealthPotion(int healthBoost) {
        putBoostMap(Stat.CURRENT_HEALTH, healthBoost);
    }
}
