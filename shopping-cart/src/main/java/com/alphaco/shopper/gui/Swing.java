package com.alphaco.shopper.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import static com.alphaco.shopper.gui.Utils.getProductList;

public class Swing extends JFrame {

    protected static Container c;
    protected static JScrollPane cartScrollPanel;
    protected static JScrollPane productsScrollPane;
    protected static Font titleFont;
    protected static Font brandFont;
    protected static Font descFont;
    protected static Font infoFont;
    protected static JPanel configPanel;
    protected static JPanel cartClientView;
    protected static int oldWindowHeight;
    protected static int oldWindowWidth;
    protected static int oldNumCols;
    protected static int scrollBarWidth;
    protected static int configPanelHeight;
    protected static int cartScrollPanelWidth;

    private ConfigPanel myConfigPanel;
    private CartPanel cartPanel;
    private ProductsPanel productsPanel;



    public Swing() {
        setupFrame();
        initializeVariables();
        addListeners();
        initializeComponents();
        setVisible(true);
    }

    private void setupFrame() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException
                | InstantiationException
                | IllegalAccessException
                | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        setTitle("ShoppingCart");
        setSize(1500, 1000);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        c = getContentPane();
        c.setLayout(null);
    }

    private void initializeVariables() {
        scrollBarWidth = ((int) UIManager.get("ScrollBar.width"));
        configPanel = new JPanel();
        cartClientView = new JPanel();
        configPanel = new JPanel();
        cartClientView = new JPanel();
        cartScrollPanel = new JScrollPane();
        productsScrollPane = new JScrollPane();
        titleFont = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
        brandFont = new Font(Font.SANS_SERIF, Font.BOLD, 15);
        descFont = new Font(Font.SANS_SERIF, Font.PLAIN, 15);
        infoFont = new Font(Font.SANS_SERIF, Font.BOLD, 10);
        oldWindowHeight = 1000;
        oldWindowWidth = 1000;
        configPanelHeight = 100;
        cartScrollPanelWidth = 260;
    }

    private void initializeComponents() {
        cartPanel = new CartPanel(this);
        productsPanel = new ProductsPanel(this);
        myConfigPanel = new ConfigPanel(this, productsPanel, cartPanel);
    }

    public static void main(String[] args) {
        new Swing();
    }

    public void start() {
        cartPanel.requestCart();
        productsPanel.injectProducts(getProductList(), myConfigPanel);
    }

    private void addListeners() {
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                int y = getHeight();
                int x = getWidth();

                if (y != oldWindowHeight) {
                    cartPanel.repaintCartPanelSize();
                    productsPanel.repaintProductPanelSize();
                    oldWindowHeight = y;
                }
                if (x != oldWindowWidth) {
                    myConfigPanel.repaintPanel();
                    cartPanel.repaintCartPanelLocation();
                    productsPanel.repaintPanel();
                    oldWindowWidth = x;
                }
            }
        });
    }
}
