package alpha.game;

public enum Pics {
    NEW(0), HEAD(1), BODY1(2), BODY2(3), ARM1(4), ARM2(5), LEG1(6), LEG2(7);

    Pics(int i) {
    }

    public void getPic() {
        switch (this) {
            case NEW:
                Printer.hangStart();
                break;
            case HEAD:
                Printer.hangHead();
                break;
            case BODY1:
                Printer.hangTorso();
                break;
            case BODY2:
                Printer.hangAbdomen();
                break;
            case ARM1:
                Printer.hangLeftArm();
                break;
            case ARM2:
                Printer.hangRightArm();
                break;
            case LEG1:
                Printer.hangLeftLeg();
                break;
            case LEG2:
                Printer.hangRightLeg();
                break;
        }
    }
}
