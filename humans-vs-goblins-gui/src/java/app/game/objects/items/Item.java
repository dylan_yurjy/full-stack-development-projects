package game.objects.items;

import game.Environment;
import game.objects.placeable.Human;
import game.utils.enums.HudComponent;
import game.utils.enums.InventorySlot;
import game.utils.enums.Stat;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public abstract class Item {
    private final HashMap<Stat, Integer> boostMap = new HashMap<>();
    private Canvas itemCanvas;
    private InventorySlot slot;
    private PaintListener paintListener;
    private MouseListener mouseListener;
    private PaintListener borderPaintListener;
    private MouseTrackListener hoverMouseListener;
    private boolean consumable = false;
    private Environment env;

    {
        env = Environment.human.getEnv();
        setMouseListener(initMouseListener());
        setBorderPaintListener(initBorderPaintListener());
        setHoverMouseListener(initMouseHoverListener());
    }

    public Item() {
        this.slot = InventorySlot.values()[Environment.human.getItems().size()];
    }


    public Canvas generateItemCanvas(Environment env, Image itemImage) {
        Canvas canvas = new Canvas(env.getWidgetMap().get(HudComponent.INVENTORY_CANVAS), SWT.NONE);
        canvas.setSize(60, 60);
        canvas.setBackground(env.getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));

        Label imageLabel = new Label(canvas, SWT.CENTER);
        imageLabel.setImage(itemImage);
        imageLabel.setSize(60, 60);
        imageLabel.setBackground(env.getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
        imageLabel.addMouseListener(mouseListener);

        return canvas;
    }

    protected PaintListener initPaintListener() {
        return new PaintListener() {
            @Override
            public void paintControl(PaintEvent e) {
                e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW));
                e.gc.drawRectangle(0, 0, itemCanvas.getSize().x - 1, itemCanvas.getSize().y - 1);
            }
        };
    }

    protected PaintListener initBorderPaintListener() {
        return new PaintListener() {
            @Override
            public void paintControl(PaintEvent e) {
                Label label = (Label) itemCanvas.getChildren()[0];
                e.gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
                e.gc.drawRectangle(0, 0, label.getSize().x - 1, label.getSize().y - 1);
            }
        };
    }

    protected MouseTrackListener initMouseHoverListener() {
        return new MouseTrackListener() {
            @Override
            public void mouseEnter(MouseEvent e) {
                Label label = (Label) itemCanvas.getChildren()[0];
                label.addPaintListener(borderPaintListener);
                label.redraw();
            }

            @Override
            public void mouseExit(MouseEvent e) {
                Label label = (Label) itemCanvas.getChildren()[0];
                label.removePaintListener(borderPaintListener);
                label.redraw();
            }

            @Override
            public void mouseHover(MouseEvent e) {
            }
        };
    }

    protected MouseListener initMouseListener() {
        return new MouseListener() {
            @Override
            public void mouseDoubleClick(MouseEvent e) {

            }

            @Override
            public void mouseDown(MouseEvent e) {
                if (isConsumable()) {
                    consumeItem();
                } else {
                    swapWeapon();
                }
            }

            @Override
            public void mouseUp(MouseEvent e) {

            }
        };
    }

    public void swapWeapon() {
        Human human = Environment.human;
        ArrayList<Item> items = human.getItems();
        Item item = this;
        AbstractWeapon equipped = (AbstractWeapon) human.getEquippedItem();
        Collections.swap(items, items.indexOf(item), items.indexOf(equipped));
        equipped.setSlot(InventorySlot.values()[human.getItems().indexOf(equipped)]);
        equipped.getItemCanvas().dispose();
        equipped.setItemCanvas(equipped.generateItemCanvas(human.getEnv(),  equipped.getImageRegular()));
        equipped.repositionLabel();
        activate(human);
        setSlot(InventorySlot.values()[human.getItems().indexOf(this)]);
        getItemCanvas().dispose();
        setItemCanvas(generateItemCanvas(human.getEnv(), ((AbstractWeapon) this).getImageBig()));
        repositionLabel();
    }

    public void consumeItem() {
        Human human = Environment.human;
        HashMap<Stat, Integer> humanStatsMap = human.getStatsMap();
        getBoostMap().forEach( (k, v) -> {
            humanStatsMap.put(k, humanStatsMap.get(k) + v);
        });
        getItemCanvas().dispose();
        human.getItems().remove(this);
        human.reorderSlots();
        human.getEnv().getWidgetMap().get(HudComponent.HUMAN_HEALTH_BAR).redraw();
    }

    public void putBoostMap(Stat stat, int boost) {
        boostMap.put(stat, boost);
    }

    public void repositionLabel() {
        Label imageLabel = (Label) itemCanvas.getChildren()[0];
        imageLabel.addMouseTrackListener(hoverMouseListener);
        switch (slot.ordinal()) {
            case 0:
                imageLabel.removeMouseTrackListener(hoverMouseListener);
                itemCanvas.setSize(70, 70);
                itemCanvas.setLocation((itemCanvas.getParent().getClientArea().width - itemCanvas.getSize().x) / 2, 0);
                imageLabel.setSize(70, 70);
                imageLabel.addPaintListener(paintListener);
                imageLabel.removePaintListener(borderPaintListener);
                imageLabel.removeMouseListener(mouseListener);
                imageLabel.removeMouseTrackListener(hoverMouseListener);
                itemCanvas.redraw();
                break;
            case 2:
            case 4:
            case 6:
                imageLabel.setSize(60, 60);
                itemCanvas.setLocation(250 + (((slot.ordinal() / 2) - 1) * 60), 5);
                itemCanvas.redraw();
                break;
            case 1:
            case 3:
            case 5:
                imageLabel.setSize(60, 60);
                itemCanvas.setLocation(120 - (slot.ordinal() / 2) * 60, 5);
                itemCanvas.redraw();
                break;
        }
    }

    public void initiateCanvas(Image image) {
        setItemCanvas(generateItemCanvas(env, image));
        repositionLabel();
    }

    public abstract void activate(Human human);

    public InventorySlot getSlot() {
        return slot;
    }

    public void setSlot(InventorySlot slot) {
        this.slot = slot;
    }

    public Canvas getItemCanvas() {
        return itemCanvas;
    }

    public void setItemCanvas(Canvas itemCanvas) {
        this.itemCanvas = itemCanvas;
    }

    public HashMap<Stat, Integer> getBoostMap() {
        return boostMap;
    }

    public boolean isConsumable() {
        return consumable;
    }

    public void setConsumable(boolean consumable) {
        this.consumable = consumable;
    }

    public void setPaintListener(PaintListener paintListener) {
        this.paintListener = paintListener;
    }

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public void setMouseListener(MouseListener mouseListener) {
        this.mouseListener = mouseListener;
    }

    public void setBorderPaintListener(PaintListener borderPaintListener) {
        this.borderPaintListener = borderPaintListener;
    }

    public void setHoverMouseListener(MouseTrackListener hoverMouseListener) {
        this.hoverMouseListener = hoverMouseListener;
    }
}
