import cheerio from "cheerio";
import fs from "fs";
import fetch from "node-fetch";

const getRawData = (URL) => {
    return fetch(URL)
        .then(res => res.text())
        .then(data => data)
}

const getInterviewData = async () => {

    const URL = "https://www.javatpoint.com/interview-questions-and-answers"

    const BaseURL = "https://www.javatpoint.com/"

    const javatInterviewDashData = await getRawData(URL);
    const loadedRawHTML = cheerio.load(javatInterviewDashData);

    const interviewCategoriesTD = loadedRawHTML("#city").find('fieldset')

    let urls = [];

    interviewCategoriesTD.each((i, e) => {
        let extract = false;
        let category;
        e.children.forEach( e1 => {
            if (e1['name'] === 'h2') {
                let text = e1['children'][0]['data']
                if (text === "Java" || text === "Web" || text === 'Database') {
                    category = text;
                    extract = true;
                }
            }
        })

        if (extract) {
            e['children'][3]['children'].forEach( e1 => {
                if (e1.name === 'a') {
                    let path = e1['attribs']['href']
                    let subCategory;
                    e1.children.forEach( e2 => {
                        if (e2.name === 'div') {
                            e2['children'].forEach( e3 => {
                                if (e3.name === 'p') {
                                    subCategory = e3.children[0].data
                                }
                            })
                        }
                    })
                    urls.push({category, subCategory, path: BaseURL + path});
                }
            })
        }
    })

    console.log(urls)

    fs.writeFile("./data.json", JSON.stringify(urls),
        {
            encoding: 'utf-8'
        },
        (err) => {
            if (err) {
                console.log(err)
            }
        });
}

getInterviewData();