package alpha.game;

public class Printer {
    public static void hangStart() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangHead() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangTorso() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |        |        *");
        System.out.println("*      |                 *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangAbdomen() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |        |        *");
        System.out.println("*      |        |        *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangLeftArm() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |       /|        *");
        System.out.println("*      |        |        *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangRightArm() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |       /|\\       *");
        System.out.println("*      |        |        *");
        System.out.println("*      |                 *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangLeftLeg() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |       /|\\       *");
        System.out.println("*      |        |        *");
        System.out.println("*      |       /         *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
    public static void hangRightLeg() {
        System.out.println("**************************");
        System.out.println("*      ,________,        * Try to guess the whole word by appending an exclamation point at the end. If you're incorrect, you lose.\");");
        System.out.println("*      |        |        *");
        System.out.println("*      |        O        *");
        System.out.println("*      |       /|\\       *");
        System.out.println("*      |        |        *");
        System.out.println("*      |       / \\       *");
        System.out.println("*   ___|___              *");
        System.out.println("**************************");
        System.out.print("PROGRESS-> ");
        for (var letter : Config.getPlayerWord().toCharArray()) {
            System.out.print(String.valueOf(letter).toUpperCase() + " ");
        }
        System.out.print("GUESSES -> ");
        for (var letter : Config.getGuessMap().get("totalGuesses")) {
            System.out.print(letter.toUpperCase() + "-");
        }
        System.out.println();
    }
}
