package game;

import game.ai.GoblinAI;
import game.objects.placeable.Drop;
import game.objects.placeable.GameObject;
import game.utils.Combat;
import org.eclipse.swt.SWTException;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Land {

    private final GameObject[][] land;
    private final Environment env;

    public Land(Environment env) {
        this.env = env;
        int rowLength = 15;
        int columnLength = 15;
        land = new GameObject[columnLength][rowLength];
        for (GameObject[] gameObjects : land) {
            Arrays.fill(gameObjects, null);
        }
    }

    public void updateLand() throws SWTException {
        nullifyCoordinates(env.getOldLocations());
        ArrayList<GameObject> gameObjs = env.getGameObjects();
        for (GameObject obj : gameObjs) {
            Point loc = obj.getLocation();
            int column = loc.x;
            int row = loc.y;
            land[row][column] = obj;
            obj.getLabel().setLocation(loc.x * 50, loc.y * 50);
        }
    }

    public void checkForDropCollection() {
        Iterator<GameObject> iter = env.getGameObjects().iterator();
        while (iter.hasNext()) {
            GameObject obj = iter.next();
                if (obj instanceof Drop) {
                    if (obj.getLocation().equals(Environment.human.getLocation())) {
                        Environment.human.acquireDropContents(env, (Drop) obj);
                        iter.remove();
                    }
            }
        }
    }

    public boolean checkCollided() {
        return GoblinAI.checkForCollision();
    }

    public void checkForCombatInitiation() {
        if (GoblinAI.checkForCollision()) {
            Combat.setHumanInCombat(true);
            Combat.initiateCombat();
        }
    }

    public void nullifyCoordinates(ArrayList<Point> list) {
        if (list.size() > 0) {
            for (var obj : list) {
                Point loc = obj.getLocation();
                int column = loc.x;
                int row = loc.y;
                land[row][column] = null;
            }
            list.clear();
        }
    }

    public GameObject[][] getLand() {
        return land;
    }
}
