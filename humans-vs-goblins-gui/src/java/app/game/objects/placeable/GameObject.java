package game.objects.placeable;

import game.Environment;
import game.utils.enums.Direction;
import game.utils.enums.HudComponent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Label;

import java.awt.*;
import java.util.Random;

public class GameObject {
    private final Label label;
    private final Point location;
    private final Environment env;

    public GameObject(Environment env, int x, int y, Image rep) {
        location = new Point(x, y);
        this.env = env;
        this.label = new Label(env.getWidgetMap().get(HudComponent.LAND_CANVAS), SWT.NONE);
        label.setSize(50, 50);
        label.setImage(rep);
        label.setBackground(env.getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
    }

    public GameObject(Environment env, Image rep) {
        this.env = env;
        location = generateRandomLocation();
        this.label = new Label(env.getWidgetMap().get(HudComponent.LAND_CANVAS), SWT.NONE);
        label.setSize(50, 50);
        label.setImage(rep);
        label.setBackground(env.getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT));
    }

    public void addPreviousLocation() {
        env.getOldLocations().add(new Point(location));
    }

    public void move(Direction dir) {
        switch (dir) {
            case UP:
                if (location.getY() != 0) {
                    addPreviousLocation();
                    location.setLocation(location.getX(), location.getY() - 1);
                }
                break;
            case DOWN:
                if (location.getY() != 14) {
                    addPreviousLocation();
                    location.setLocation(location.getX(), location.getY() + 1);
                }
                break;
            case LEFT:
                if (location.getX() != 0) {
                    addPreviousLocation();
                    location.setLocation(location.getX() - 1, location.getY());
                }
                break;
            case RIGHT:
                if (location.getX() != 14) {
                    addPreviousLocation();
                    location.setLocation(location.getX() + 1,location.getY());
                }
                break;
        }
    }

    private Point generateRandomLocation() {
        Random rand = new Random();
        int row = rand.nextInt(15);
        int column = rand.nextInt(15);
        if (this instanceof Goblin) {
            while (env.getLand().getLand()[row][column] != null || ((row != 0 && row != 14) || (column != 0 && column != 14))) {
                row = rand.nextInt(15);
                column = rand.nextInt(15);
            }
        } else {
            while (env.getLand().getLand()[row][column] != null) {
                row = rand.nextInt(15);
                column = rand.nextInt(15);
            }
        }
        return new Point(column, row);
    }

    public Label getLabel() {
        return label;
    }

    public Point getLocation() {
        return location;
    }

    public int getLocationX() {
        return location.x;
    }

    public int getLocationY() {
        return location.y;
    }

    public Environment getEnv() {
        return env;
    }
}
