package hangman;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Variables {

    /*
    Statically initialized variables for use in managing the state of current Hangman game.
     */

    public static Scanner scanner = new Scanner(System.in);
    public static HashMap<String, ArrayList<String>> guesses = new HashMap<>();
    public static ArrayList<String> resources;
    public static ArrayList<String> scores;
    public static int stage = 0;
    public static String guess = "";
    public static String computerWord;
    public static String playerWord = "";
    public static String name;
    public static Boolean hasWon = null;

    static {
        guesses.put("totalGuesses", new ArrayList<>());
        guesses.put("correctGuesses", new ArrayList<>());
    }
}
