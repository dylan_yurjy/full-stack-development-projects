import java.io.IOError;
import java.util.Scanner;


public class Main {

    double random;
    int number = 0;
    Integer guess = 0;
    static int guesses = 0;
    static String name;
    String replay;
    boolean acceptable = false;
    Scanner scanner = new Scanner(System.in);

    public Main() {
        System.out.println(response(Res.WELCOME));;
        Main.name = scanner.nextLine();
        start();
    }

    private void start() {
        generator();
        System.out.println(response(Res.START));
        while (guess == null || guess != number) {
            try {
                guess = (int) Integer.parseInt(scanner.nextLine());
                Main.guesses++;
            } catch (IOError e) {
                System.out.println("There was an error");
            } catch (NumberFormatException e1) {
                guess = null;
                System.out.println("Do not use letters.");
            }
            if (guess == null) {
                // do nothing
            } else if (guess > number) {
                System.out.println(response(Res.LOW));
            } else if (guess < number) {
                System.out.println(response(Res.HIGH));
            }
        }
        System.out.println(response(Res.VICTORY));
        while (acceptable == false) {
            replay = scanner.nextLine();
            if (replay.equals("n") || replay.equals("y")) {
                acceptable = true;
            } else {
                System.out.println("Try Again\n");
            }
        }
        if (replay.equals("n")) {
            // Do nothing
        } else {
            restart();
        }

    }

    private void generator() {
        while (number > 20 || number < 1) {
            random = Math.random();
            number = Integer.parseInt(String.format("%.3f", random).toString().substring(2,4));
        }
    }

    private void restart() {
        guess = 0;
        number = 0;
        Main.guesses = 0;
        acceptable = false;
        replay = "";
        start();
    }

    private String response(Res res) {
        return res.getRes();
    }

    static String responseTest(Res res) {
        return res.getRes();
    }

    public static void main(String[] args) {
        new Main();
    }
}
