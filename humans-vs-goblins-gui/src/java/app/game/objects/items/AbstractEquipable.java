package game.objects.items;

import game.objects.placeable.Human;
import game.utils.enums.Stat;

import java.util.HashMap;

public abstract class AbstractEquipable extends Item {


    {
        setPaintListener(initPaintListener());
    }

    public void activate(Human human) {
        equip(human);
    }

    public void equip(Human human) {
        HashMap<Stat, Integer> humanStats = human.getStatsMap();
        if (human.getEquippedItem() == null) {
            human.setEquippedItem(this);
        } else {
            HashMap<Stat, Integer> oldItemBoostMap = human.getEquippedItem().getBoostMap();
            oldItemBoostMap.forEach( (k, v) -> humanStats.put(k, humanStats.get(k) - v));
            human.setEquippedItem(this);
        }
        getBoostMap().forEach( (k, v) -> humanStats.put(k, humanStats.get(k) + v));

    }
}
