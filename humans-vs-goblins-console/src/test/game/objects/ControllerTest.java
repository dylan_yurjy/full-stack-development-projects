package game.objects;

import game.utils.InputProcessor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class ControllerTest {

    Environment env = new Environment(true);
    InputProcessor inputProcessor = env.getInputProcessor();

    @ParameterizedTest
    @ValueSource(strings = {"a", "s", "w", "d", "s", "w", "a" , "s", "w" , "d"})
    @DisplayName("Correct Input Validation Test")
    void correctInputValidationTest(String word) {
        assertTrue(inputProcessor.validateInput(word), "Error in function. Correct input was flagged as incorrect");
    }

    @ParameterizedTest
    @ValueSource(strings = {"hello", "t", "Blue", "123", "five", "z", "dd", "ss"})
    @DisplayName("Incorrect Input Validation Test")
    void incorrectInputValidationTest(String word) {
        assertFalse(inputProcessor.validateInput(word), "Error in function. Incorrect input was flagged as correct");
    }
}
