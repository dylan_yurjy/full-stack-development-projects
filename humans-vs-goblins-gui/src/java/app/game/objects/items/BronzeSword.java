package game.objects.items;

import game.Environment;
import org.eclipse.swt.graphics.Image;

public class BronzeSword extends AbstractWeapon {

    public BronzeSword(Environment env) {
        super(15, "Bronze Sword", env.getImageLoader().getBronzeSwordIcon(), env.getImageLoader().getBronzeSwordBigIcon());
    }
}
