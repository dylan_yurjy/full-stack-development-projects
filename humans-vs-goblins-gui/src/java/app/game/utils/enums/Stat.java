package game.utils.enums;

public enum Stat {
    CURRENT_HEALTH, MAX_HEALTH, STRENGTH
}
