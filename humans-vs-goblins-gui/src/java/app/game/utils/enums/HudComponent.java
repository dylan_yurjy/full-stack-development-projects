package game.utils.enums;

public enum HudComponent {
    LAND_CANVAS, HUD_CANVAS, HUMAN_HEALTH_BAR, COINS_CANVAS, SKULLS_CANVAS, INVENTORY_CANVAS
}
