import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class DragonCaveTest {

    @BeforeEach
    void setUp() {
        System.out.println("Beginning All Tests. Be prepared.");
    }

    @Test
    void testResponse() {
        assertEquals("You are in a land full of dragons. In front of you,\n\nyou see two caves. In one cave, " +
                "the dragon is friendly\n\nand will share his treasure with you. The other dragon\n\nis greedy and " +
                "hungry and will eat you on sight.\n\nWhich cave will you go into? (1 or 2)\n", DragonCave.responseTest(Res.START), "Test Failed");
    }

    @AfterEach
    void tearDown() {
        System.out.println("Testing is now completed. Please review the results.");
    }


}