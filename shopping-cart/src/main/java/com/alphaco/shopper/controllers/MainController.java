package com.alphaco.shopper.controllers;

import com.alphaco.shopper.entitys.CartItem;
import com.alphaco.shopper.entitys.Product;
import com.alphaco.shopper.service.UserService;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.*;

@RestController
public class MainController {

    @Autowired
    UserService userService;

    @GetMapping("/products/getAll")
    public byte[] getProducts() throws IOException {
        List<Product> prods = userService.getProducts();
        return serializer(prods);
    }

    @GetMapping("/getCart/{name}")
    public byte[] getCart(@PathVariable String name) throws IOException {
        List<CartItem> cartItems = userService.getCartContents(name);
        return serializer(cartItems);
    }

    @PostMapping("/addUser/{name}")
    public boolean addUser(@PathVariable String name) {
        userService.addUser(name);
        return true;
    }

    @PostMapping("/getCart/addProduct/{userName}/{prodId}")
    public byte[] addProductToCart(@PathVariable String userName, @PathVariable String prodId) throws IOException {
        List<CartItem> items = userService.addProductToCart(userName, prodId);
        return serializer(items);
    }

    @PutMapping("/getCart/incrementProduct/{userName}/{prodId}")
    public boolean incrementProductQuantity(@PathVariable String userName, @PathVariable String prodId) {
        return userService.incrementProductQuantity(userName, prodId);
    }

    @PutMapping("/getCart/decrementProduct/{userName}/{prodId}")
    public boolean decrementProductQuantity(@PathVariable String userName, @PathVariable String prodId) {
        userService.decrementProductQuantity(userName, prodId);
        return true;
    }

    @DeleteMapping("/getCart/removeProduct/{userName}/{prodId}")
    public byte[] removeProductFromCart(@PathVariable String userName, @PathVariable String prodId) throws IOException {
        List<CartItem> items = userService.removeProductFromCart(userName, prodId);
        return serializer(items);
    }

    @DeleteMapping("/getCart/{name}/removeContents")
    public boolean removeContents(@PathVariable String name) {
        return userService.clearCart(name);
    }

    @DeleteMapping("/removeUser/{name}")
    public boolean removeUser(@PathVariable String name) {
        userService.removeUser(name);
        return true;
    }

    private <T> byte[] serializer(T t) throws IOException {
        byte[] bytes;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(t);
        objectOutputStream.flush();
        bytes = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        objectOutputStream.close();
        return bytes;
    }
}
