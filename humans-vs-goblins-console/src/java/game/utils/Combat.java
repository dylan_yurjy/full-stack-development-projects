package game.utils;

import game.objects.*;

import java.util.Random;

public class Combat {

    private static boolean inCombat = false;
    private static boolean threadRunning = false;

    public static void battle(Environment env, Human human, Goblin goblin) {
        if (threadRunning) {
            // do nothing
        } else {
            Thread battleThread = new Thread() {
                @Override
                public void run() {
                    while (getInCombat()) {
                        if (goblin.getStats().get("currentHealth") <= 0 || human.getStats().get("currentHealth") <= 0) {
                            concludeBattle(env,human, goblin);
                            setInCombat(false);
                            env.updateLand();
                        } else {
                            human.attack(goblin);
                            goblin.attack(human);
                            env.getLand().getPrinter().setHudData(0, String.format("Health: %s/%s", human.getStats().get("currentHealth"), human.getStats().get("maxHealth")), true);
                            env.getLand().getPrinter().setHudData(3, String.format("Goblin Health: %s/%s", goblin.getStats().get("currentHealth"), goblin.getStats().get("maxHealth")), true);
                            env.getLand().renderLand();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    threadRunning = false;
                }
            };
            battleThread.start();
            threadRunning = true;
        }
    }

    public static void concludeBattle(Environment env, Human human, Goblin goblin) {
        if (human.getStats().get("currentHealth") <= 0) {
            env.setActive(false);
        } else if (goblin.getStats().get("currentHealth") <= 0) {
            env.getPlacedObjects().remove(goblin);
            env.getOldLocations().add(goblin.getLocation());
            env.getLand().getPrinter().setHudData(3, false);
            Drops drop = new Drops('D', goblin.getLocation().getX(), goblin.getLocation().getY(), Goblin.level);
            env.getPlacedObjects().add(drop);
            Goblin.level++;
            env.setGoblin(null);
            int spawnY = 1;
            Random rand = new Random();
            while (env.getLand().getLand()[1][spawnY] != ' ') {
                spawnY = rand.nextInt(2, 28);
            }
            env.setGoblin(Goblin.spawnGoblin(env, 1, spawnY));
            boolean spawnGold = true;
            for (var object : env.getPlacedObjects()) {
                if (object instanceof Gold) {
                    spawnGold = false;
                    break;
                }
            }
            if (spawnGold) {
                Gold.spawnTreasure(env);
            }
        }
    }

    public static boolean checkForCollision(int hX, int hY, int gX, int gY) {
        int xDif = Math.abs(hX - gX);
        int yDif = Math.abs(hY - gY);
        return ((xDif == 1 && yDif == 1) || (xDif == 1 && yDif == 0) || (xDif == 0 && yDif == 1));
    }

    public static boolean getInCombat() {
        return inCombat;
    }

    public static void setInCombat(boolean value) {
        inCombat = value;
    }
}
