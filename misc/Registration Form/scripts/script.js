var select1 = document.getElementById("inputOcc")
var select2 = document.getElementById("inputCountry")

var data = {
    "occupations": ["Athletes: Business Manager","Athletic Coach","Athletic Director","Athletic Trainer","ATM Machine Servicer","Atmospheric and Space Scientist","Audio-Visual Collections Specialist","Audiovisual Production Specialist","Automobile Mechanic","Automotive Body Repairer","Automotive Engineer","Automotive Glass Installer","Avionics Technician","Baggage Porters and Bellhops","Baker (Commercial)","Ballistics Expert","Bank and Branch Managers","Bank Examiner","Bank Teller","Benefits Manager","Bicycle Mechanic","Billing Specialist","Bindery Machine Set-Up Operators","Bindery Machine Tender","Biological Technician","Biology Professor","Biomedical Engineer","Biomedical Equipment Technician","Boat Builder","Book Editor","Border Patrol Agent","Brattice Builder","Bridge and Lock Tenders","Broadcast News Analyst","Broadcast Technician","Broker's Floor Representative","Brokerage Clerk","Budget Accountant","Budget Analyst","Building Inspector","Building Maintenance Mechanic","Bulldozer / Grader Operator","Bus and Truck Mechanics","Bus Boy / Bus Girl","Bus Driver (School)","Bus Driver (Transit)","Business Professor","Business Service Specialist","Cabinet Maker","Camp Director","Caption Writer","Cardiologist (MD)","Cardiopulmonary Technologist","Career Counselor","Cargo and Freight Agents","Carpenter's Assistant","Carpet Installer","Cartographer (Map Scientist)"],

    "countries": ["Afghanistan","Albania","Algeria","Andorra","Angola","Antigua & Deps","Argentina","Armenia","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bhutan","Bolivia","Bosnia Herzegovina","Botswana","Brazil","Brunei","Bulgaria","Burkina","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Central African Rep","Chad","Chile","China","Colombia","Comoros","Congo","Congo {Democratic Rep}","Costa Rica","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Fiji","Finland","France","Gabon","Gambia","Georgia","Germany","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland {Republic}","Israel","Italy","Ivory Coast","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","Korea North","Korea South","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Morocco","Mozambique","Myanmar, {Burma}","Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Qatar","Romania","Russian Federation","Rwanda","St Kitts & Nevis","St Lucia","Saint Vincent & the Grenadines","Samoa","San Marino","Sao Tome & Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Sudan","Spain","Sri Lanka","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Yemen","Zambia","Zimbabwe"]
    }

data.occupations.forEach(element => {
    var opt = document.createElement("option");
    opt.value = element
    opt.innerHTML = element
    select1.appendChild(opt);
});

data.countries.forEach(element => {
    var opt = document.createElement("option");
    opt.value = element
    opt.innerHTML = element
    select2.appendChild(opt);
});

function processData() {
    let emailTag = "<strong>Email: </strong>";
    let email = document.getElementById("inputEmail").value;
    let passwordTag = "<strong>Password: </strong>"
    let password = document.getElementById("inputPassword").value;
    let passwordString = "";
    for (i = 0; i < password.length; i++) {
        passwordString += "*";
    }
    let occupationTag = "<strong>Occupation: </strong>"
    let occupation = document.getElementById("inputOcc").value;
    let countryTag = "<strong>Country: </strong>"
    let country = document.getElementById("inputCountry").value;
    let paragraph = document.createElement("p");
    let newline = "<br>"
    paragraph.className = "results-display";
    paragraph.innerHTML = emailTag + email + newline + passwordTag + passwordString + newline + occupationTag + occupation + newline + countryTag + country;
    document.getElementById("hello").appendChild(paragraph);
}