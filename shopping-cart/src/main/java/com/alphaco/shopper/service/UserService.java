package com.alphaco.shopper.service;

import com.alphaco.shopper.entitys.CartItem;
import com.alphaco.shopper.entitys.Product;

import java.util.List;

public interface UserService {
    void addUser(String name);
    void removeUser(String name);
    List<CartItem> getCartContents(String name);
    List<CartItem> addProductToCart(String userName, String prodId);
    List<CartItem> removeProductFromCart(String userName, String prodId);
    boolean incrementProductQuantity(String userName, String prodId);
    void decrementProductQuantity(String userName, String prodId);
    boolean clearCart(String userName);
    List<Product> getProducts();
}
