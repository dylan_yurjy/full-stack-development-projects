package game.objects;

import game.exceptions.BoundsException;
import game.exceptions.OccupiedSpaceException;

public class Goblin extends Creature {

    public static int level = 1;

    public Goblin(int x, int y) {
        super('G', x, y);
    }

    @Override
    public String toString() {
        return String.valueOf(super.getRepresentation());
    }

    public static Goblin spawnGoblin(Environment env, int x, int y) {
        Goblin goblin;
        try {
            env.getLand().validateLocation(x, y);
        } catch (BoundsException | OccupiedSpaceException e) {
            e.printStackTrace();
            return null;
        }
        goblin = new Goblin(x, y);
        env.getPlacedObjects().add(goblin);
        return goblin;
    }
}
