package hangman;

import static hangman.Variables.*;
import static hangman.Main.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Functions {
    /*
    Imports the artwork and list of words from the art.txt file located in the project resource file. This function will set
    an ArrayList equal to the contents of the file where the 0th index is a String containing all the words each separated by a comma.
    All succeeding indexes (from 1 to 6) will contain the artwork to display the current stage which indicates how many incorrect guesses
    the user has made.

    This function will also import a list of scores made by previous users when playing Hangman and then assign those scores
    to an ArrayList called scores.
     */
    public static void importData() throws IOException {
        resources = (ArrayList<String>) Arrays.stream(new String(Objects.requireNonNull(Main.class.getResourceAsStream("/art.txt")).readAllBytes(), StandardCharsets.UTF_8).split("~~~")).collect(Collectors.toList());
        scores = (ArrayList<String>) Arrays.stream(new String(Main.class.getResourceAsStream(("/scores.txt")).readAllBytes(), StandardCharsets.UTF_8).split("\\r\\n")).collect(Collectors.toList());
    }



    public static void importDataTest() {

    }

    /*
    Generates a random index to supply the chooseSecretWord function. The list of words contained in this project is of
    length 7 and the index returned will be between 0 and 6 inclusive.
     */
    public static int generateRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(7);
    }

    /*
    Splits the String variable of words, which are separated by commas, into a List of words and then uses the generateRandomIndex
    function to extract a word from a random index.
     */
    public static String chooseSecretWord(int randomIndex) {
        return Arrays.asList(resources.get(0).split(",")).get(randomIndex);
    }

    /*
    Uses the map function to initialize the player's accumulative word. Each letter in the secret word is mapped to an
    underscore and then reduced to a single String variable.
     */
    public static String generatePlayerWord() {
        return Arrays.stream(computerWord.split("")).map(e -> "_").reduce("", (a, b) -> a + b);
    }

    /*
    Asks the user to input their name and returns the value.
     */
    public static String retrieveName() {
        System.out.println("Welcome to Hangman Functional. What's your name?");
        return scanner.nextLine();
    }

    /*
    Displays the beginning title of the game including the users name.
     */
    public static String retrieveStartTitle() {
        return String.format("Okay, %s, lets begin.", name);
    }

    /*
    Uses the console to display the current stage of Hangman the user is currently at. The first index of the resources variable
    is a list of words so the index is calculated by adding one to the current stage value. All whitespace is stripped from
     the ends of the String so there are no excess lines above or below the artwork.
     */
    public static String retrieveStageArtwork() {
        return resources.get(stage + 1).strip();
    }

    /*
    Uses the reduce function to create a single string of all the users current guesses then returns the String to be displayed
    by System.out.
     */
    public static String retrieveUserGuesses() {
        return String.format("Your current guesses: %s", guesses.get("totalGuesses").stream().reduce("", (a, b) -> a + " " + b).toUpperCase());
    }

    /*
    Basic getter to retrieve the player's accumulated word.
     */
    public static String retrievePlayerWord() {
        return String.format("Your progress: %s", playerWord);
    }

    /*
    Neatly prints out the current stage, the users current list of guessed letters, and the users current accumulated word
    each on their own line.
     */
    public static void displayInformation() {
        System.out.println(retrieveStageArtwork());
        System.out.println(retrieveUserGuesses());
        System.out.println(retrievePlayerWord());
    }

    /*
    A functional do-while loop that asks for the user to input a letter and then assigns the input to a static guess variable.
    If the input is not a single letter between a-z (case-insensitive) then the function is called again.
     */
    public static void receiveGuess() {
        guess = scanner.nextLine().toLowerCase();
        Pattern pattern = Pattern.compile("(?<!.)\\b[a-z](?!.+)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(guess);
        if (!matcher.find()) {
            System.out.println("Incorrect Format. Try Again.");
            receiveGuess();
        }
    }

    /*
    Checks if the validated input received by the user is contained in the users list of current guesses. If the current
    guess is contained in the list of guesses, the function will return true, otherwise false.
     */
    public static boolean checkRepeated() {
        if (guesses.get("totalGuesses").contains(guess)) {
            System.out.println("You already guessed that!");
            return true;
        } else {
            guesses.get("totalGuesses").add(guess);
            return false;
        }
    }

    /*
    If the checkRepeated function returns false, this function will check if the users guess is contained in the secret word.
    If the guess is contained in the secret word, the guess will be added to a list of correct guesses and then the player's
    accumulated word will be updated to reflect the correct guess. If the guess is not contained in the secret word the stage is
    incremented by one.
     */
    public static void checkContains() {
        if (computerWord.contains(guess)) {
            guesses.get("correctGuesses").add(guess);
            playerWord = updatePlayerWord();
        } else {
            stage++;
        }
    }

    /*
    Uses the map function to generate the player's accumulated word by mapping each letter in the secret word to the letter
    itself or an underscore depending on if the letter is contained in the list of correct guesses made by the user.
     */
    public static String updatePlayerWord() {
        return Arrays.stream(computerWord.split("")).map(e -> guesses.get("correctGuesses").contains(e) ? e : "_").reduce("", (a, b) -> a + b).toUpperCase();
    }

    /*
    After a valid guess this function will analyze whether the user has won or lost. If the player's accumulated word is
    equal to the secret word, a boolean variable hasWon will be set to true. If the stage has been incremented to a value of 7
    then hasWon will be set to false. If neither of these conditions are met, the function will do nothing.
     */
    public static void analyzeProgress() {
        if (playerWord.equalsIgnoreCase(computerWord)) {
            hasWon = true;
        } else {
            if (stage == 7) {
                hasWon = false;
            }
        }
    }

    /*
    Calculates the high score from the list of scores and compares the value to the users score for this round. If the user's
    score is equal to or lower than the lowest par, the function will return true indicating the user has achieved the highest score compared to
    all scores recorded so far.
     */
    public static boolean checkHighScore() {
        ArrayList<Integer> listOfScores = (ArrayList<Integer>) scores.stream().map(e -> {
            if (e.contains("Lost")) {
                return 7;
            } else {
                return Integer.parseInt(String.valueOf(e.charAt(e.length() - 1)));
            }
        }).collect(Collectors.toList());
        return stage - 1 <= Collections.min(listOfScores);
    }

    /*
    Generates a String entry to be appended to the list of scores file.
     */
    public static String generateScoreEntry() {
        String entry;
        if (hasWon) {
            entry = String.format("%s - Won - Par: %s", name, stage);
        } else {
            entry = String.format("%s - Lost", name);
        }
        scores.add(entry);
        return String.format("\r\n%s", entry);
    }

    /*
    Writes the user's score to a score.txt file.
     */
    public static void writeScoreToFile() throws IOException {
        String entry = generateScoreEntry();
        String localDir = System.getProperty("user.dir");
        File file = new File(localDir + "//src//java//resources//scores.txt");
        FileWriter writer = new FileWriter(file, true);
        writer.write(entry);
        writer.close();
    }

    /*
    If the user has won or lost this function will display the results and then ask the user if they would like to play again.
    If the user responds yes, the current state of the game will be reset. If the user responds no, the system will exit.
     */
    public static void gameOver() throws IOException {
        writeScoreToFile();
        if (hasWon) {
            System.out.println("Congratulations! You have won the game! :)");
            if (checkHighScore()) {
                System.out.println("You have the highest score!");
            }
        } else {
            System.out.println("Sorry, you lose :(");
        }
        scores.stream().forEach(System.out::println);
        while (hasWon != null) {
            System.out.println("Play again?");
            guess = scanner.nextLine();
            if (guess.equalsIgnoreCase("y")) {
                stage = 0;
                guesses.replaceAll((k, v) -> new ArrayList<>());
                hasWon = null;
            } else if (guess.equalsIgnoreCase("n")) {
                System.exit(0);
            }
            System.out.println("Incorrect Response.");
        }
        play();
    }
}
