package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class Controller {

    @Autowired
    FoodService foodService;

    @PostMapping("/")
    public boolean home(@RequestBody Map<String, Object> map) {
        System.out.println(map);
        foodService.addItem("Hello");
        return true;
    }
}
